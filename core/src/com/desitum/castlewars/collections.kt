package com.desitum.castlewars

import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.desitum.castlewars.model.Player
import com.desitum.castlewars.view.Card
import com.desitum.library.view.View
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by kodyvanry on 7/29/17.
 */

fun Collection<View>.findById(id: Int) : View {
    return find { it.id == id }!!
}

fun <T> Array<T>.chooseRandom() : T {
    return get(Random().randInt(0, size - 1))
}

fun <T> List<T>.shuffle() {
    Collections.shuffle(this)
}

fun ArrayList<Card>.add(element: Card, amount: Int) {
    add(element)
    for (i in 2..amount) {
        this.add(element.clone())
    }
}

///**
// * Performs the given [action] on each element.
// */
//public inline fun <T> Array<out T>.chooseRandom(action: (T) -> Unit): Unit {
//    for (element in this) action(element)
//}

/**
 * Returns a pseudo-random number between min and max, inclusive.
 * The difference between min and max can be at most
 * <code>Integer.MAX_VALUE - 1</code>.
 *
 * @param min Minimum value
 * @param max Maximum value.  Must be greater than min.
 * @return Integer between min and max, inclusive.
 * @see java.util.Random#nextInt(int)
 */
fun Random.randInt(min: Int, max: Int) : Int {

    // nextInt is normally exclusive of the top value,
    // so add 1 to make it inclusive
    return nextInt((max - min) + 1) + min
}

/**
 * Returns a pseudo-random number between min and max, inclusive.
 * The difference between min and max can be at most
 * <code>Integer.MAX_VALUE - 1</code>.
 *
 * @param min Minimum value
 * @param max Maximum value.  Must be greater than min.
 * @return Integer between min and max, inclusive.
 * @see java.util.Random#nextInt(int)
 */
fun Random.randFloat(min: Float, max: Float) : Float {
    return Math.random().toFloat() * (max - min) + min
}

fun BitmapFont.clone() : BitmapFont {
    return BitmapFont(this.data.fontFile, this.region)
}

/**
 * Performs the given [action] on each element.
 */
inline fun Iterable<Card>.noneAvailable(player: Player): Boolean {
    var available = false
    forEach { card ->
        if (player.canAfford(card))
            available = true
    }
    return !available;
}