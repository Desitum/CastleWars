package com.desitum.castlewars.persistence

/**
 * Created by kodyvanry on 8/16/17.
 */
interface ShoppingResponseListener {
    fun onShoppingResponse(skus: List<Purchase>)
    fun onPurchaseResponse(sku: String)
}