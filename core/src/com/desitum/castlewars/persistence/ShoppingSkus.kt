package com.desitum.castlewars.persistence

/**
 * Created by kodyvanry on 8/16/17.
 */
object ShoppingSkus {
    @JvmStatic val EXTRA_SLOT_1 = "extra_slot_1_id"
    @JvmStatic val EXTRA_SLOT_2 = "extra_slot_2_id"
    @JvmStatic val FLAME_PACK = "flame_card_pack_id"
    @JvmStatic val JAPANESE_PACK = "japanese_card_pack_id"
}