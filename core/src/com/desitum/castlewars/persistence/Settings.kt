package com.desitum.castlewars.persistence

/**
 * Created by kodyvanry on 7/31/17.
 */
class Settings {

    companion object {
        var HAND_SIZE = 6
        var flamePackPurchased = false
        var japanesePackPurchased = false
        var extraSlot1Purchased = false
        var extraSlot2Purchased = false

        fun setPurchases(purchases: List<Purchase>) {
            purchases.forEach {
                addPurchase(it.sku)
                HAND_SIZE = 6 + (if (extraSlot1Purchased) 1 else 0) + (if (extraSlot2Purchased) 1 else 0)
            }
        }

        fun addPurchase(sku: String) {
            if (sku == ShoppingSkus.FLAME_PACK)
                flamePackPurchased = true
            else if (sku == ShoppingSkus.JAPANESE_PACK)
                japanesePackPurchased = true
            else if (sku == ShoppingSkus.EXTRA_SLOT_1)
                extraSlot1Purchased = true
            else if (sku == ShoppingSkus.EXTRA_SLOT_2)
                extraSlot2Purchased = true
            HAND_SIZE = 6 + (if (extraSlot1Purchased) 1 else 0) + (if (extraSlot2Purchased) 1 else 0)
        }
    }

}