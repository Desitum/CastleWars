package com.desitum.castlewars.persistence

/**
 * Created by kodyvanry on 8/16/17
 */
interface GameInterface {

    fun makePurchase(sku: String)

    fun getPurchases() : List<Purchase>

    fun setShoppingResponseListener(responseListener: ShoppingResponseListener)

    fun getShoppingResponseListener() : ShoppingResponseListener

    fun hasShownCardTutorial() : Boolean

    fun hasShownGarbageCanTutorial() : Boolean

    fun hasShownDragAndDropTutorial() : Boolean

    fun hasShownResourcesTutorial() : Boolean

    fun isVolumeOn() : Boolean

    fun turnVolumeOn()

    fun turnVolumeOff()

    fun showCardTutorial()

    fun showGarbageCanTutorial()

    fun showDragAndDropTutorial()

    fun showResourcesTutorial()

}