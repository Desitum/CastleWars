package com.desitum.castlewars.model

/**
 * Created by kodyvanry on 8/15/17.
 */
enum class Theme {
    DEFAULT,
    FLAME,
    JAPANESE
}