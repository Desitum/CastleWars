package com.desitum.castlewars.model

import com.desitum.castlewars.BasicDeck
import com.desitum.castlewars.CastleGame
import com.desitum.castlewars.add
import com.desitum.castlewars.persistence.Settings
import com.desitum.castlewars.shuffle
import com.desitum.castlewars.view.Card
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import java.util.*

/**
 * Created by kodyvanry on 6/14/17.
 */
class Deck(val world: World) {

    private var cards = ArrayList<Card>()
    private var discardPile = LinkedList<Card>()

    init {
        reload()
    }

    fun drawCard() : Card {
        if (cards.size < 2) {
            cards.addAll(discardPile)
            discardPile.clear()
            cards.shuffle()
        }
        return cards.removeAt((Math.random() * cards.size).toInt())
    }

    fun discard(card: Card) {
        discardPile.add(card)
    }

    fun addDefaultCards() {
        val assetManager = AssetManager.instance
        // WEAPON CARDS
        cards.add( Card("ASSASSIN",   CardType.WEAPON, world, "-1 RANDOM\nENEMY PERSON",            assetManager.getDrawable(CastleGame.IC_ASSASSIN), cost = 20, flags = Card.ENEMY_PEOPLE, commands = {_, enemy -> BasicDeck.assassin(enemy)}), 1 ) // ASSASSIN
        cards.add( Card("BURGLAR",    CardType.WEAPON, world, "STEAL 8 OF EACH\nENEMY RESOURCE",    assetManager.getDrawable(CastleGame.IC_BURGLAR), cost = 18,enemyGems = 8, enemyStones = 8, enemyWeapons = 8, selfGems = 8, selfStones = 8, selfWeapons = 8), 1 ) // BURGLER
        cards.add( Card("CATAPULT",   CardType.WEAPON, world, "+12 ATTACK",                         assetManager.getDrawable(CastleGame.IC_CATAPULT), cost = 24, enemyAttackEither = 12), 3 ) // CATAPULT
        cards.add( Card("LEGION",     CardType.WEAPON, world, "+16 ATTACK",                         assetManager.getDrawable(CastleGame.IC_LEGION), cost = 30, enemyAttackEither = 16), 2 ) // LEGION
        cards.add( Card("RAID",       CardType.WEAPON, world, "-24 RANDOM\nENEMY RESOURCE",         assetManager.getDrawable(CastleGame.IC_RAID), cost = 14, commands = {_, enemy -> BasicDeck.raid(enemy) }), 1 ) // RAID
        cards.add( Card("RAM",        CardType.WEAPON, world, "+6 ATTACK",                          assetManager.getDrawable(CastleGame.IC_RAM), cost = 12, enemyAttackEither = 6), 3 ) // RAM
        cards.add( Card("RECRUITER",  CardType.WEAPON, world, "+1 SOLDIER",                         assetManager.getDrawable(CastleGame.IC_RECRUITER), cost = 16, selfArmyMen = 1), 2 ) // RECRUITER
        cards.add( Card("SPEARMAN ",  CardType.WEAPON, world, "+2 ATTACK",                          assetManager.getDrawable(CastleGame.IC_SPEARMAN), cost = 4 , enemyAttackEither = 2), 3 ) // SPEARMAN
        cards.add( Card("THIEF ",     CardType.WEAPON, world, "STEAL 24 OF EACH\nENEMY RESOURCE",   assetManager.getDrawable(CastleGame.IC_THIEF), cost = 54, enemyGems = 24, enemyStones = 24, enemyWeapons = 24, selfWeapons = 24, selfGems = 24, selfStones = 24), 1 ) // THEIF
        cards.add( Card("TREBUCHET ", CardType.WEAPON, world, "+20 ATTACK",                         assetManager.getDrawable(CastleGame.IC_TREBUCHET), cost = 40, enemyAttackEither = 20), 1 ) // TREBUCHET

        // MAGIC CARDS
        cards.add( Card("BLAST",            CardType.MAGIC, world, "+8 ATTACK",          assetManager.getDrawable(CastleGame.IC_BLAST), cost = 16, enemyAttackEither = 8), 1 ) // BLAST
        cards.add( Card("CREATE GEMS",      CardType.MAGIC, world, "+16 GEMS",           assetManager.getDrawable(CastleGame.WHITE_GEM), cost = 10, selfGems = 16), 3 ) // CREATE GEMS
        cards.add( Card("CREATE STONES",    CardType.MAGIC, world, "+16 STONES",         assetManager.getDrawable(CastleGame.STONE), cost = 10, selfStones = 16), 3 ) // CREATE STONES
        cards.add( Card("CREATE WEAPONS",   CardType.MAGIC, world, "+16 WEAPONS",        assetManager.getDrawable(CastleGame.SWORD), cost = 10, selfWeapons = 16), 3 ) // CREATE WEAPONS
        cards.add( Card("DESTROY GEMS",     CardType.MAGIC, world, "-16 ENEMY\nGEMS",    assetManager.getDrawable(CastleGame.WHITE_GEM), cost = 10, enemyGems = 16), 2 ) // DESTROY GEMS
        cards.add( Card("DESTROY STONES",   CardType.MAGIC, world, "-16 ENEMY\nSTONES",  assetManager.getDrawable(CastleGame.STONE), cost = 10, enemyStones = 16), 2 ) // DESTROY STONES
        cards.add( Card("DESTROY WEAPONS",  CardType.MAGIC, world, "-16 ENEMY\nWEAPONS", assetManager.getDrawable(CastleGame.SWORD), cost = 10, enemyWeapons = 16), 2 ) // DESTROY WEAPONS
        cards.add( Card("HAT TRICK",        CardType.MAGIC, world, "+4 ALL\nRESOURCES",  assetManager.getDrawable(CastleGame.IC_TOP_HAT), cost = 4,  selfGems = 4, selfWeapons = 4, selfStones = 4), 3 )  // HAT TRICK
        cards.add( Card("LIGHTNING STRIKE", CardType.MAGIC, world, "+16 ATTACK",         assetManager.getDrawable(CastleGame.IC_LIGHTNING), cost = 32, enemyAttackEither = 16), 1 ) // LIGHTNING STRIKE
        cards.add( Card("MAGE",             CardType.MAGIC, world, "+1 WIZARD",          assetManager.getDrawable(CastleGame.WIZARD), cost = 16, selfWizards = 1), 2 ) // MAGE

        // CONSTRUCTION CARDS
        cards.add( Card("ARCHITECT",  CardType.CONSTRUCTION, world, "+1 BUILDER",                 assetManager.getDrawable(CastleGame.IC_ARCHITECT), cost = 16, selfBuilders = 1), 2 ) // ARCHITECT
        cards.add( Card("BARRIER",    CardType.CONSTRUCTION, world, "+4 WALL",                    assetManager.getDrawable(CastleGame.IC_BARRIER), cost = 8,  selfWall = 4), 3 )  // BARRIER
        cards.add( Card("CASTLE",     CardType.CONSTRUCTION, world, "+20 CASTLE",                 assetManager.getDrawable(CastleGame.IC_CASTLE), cost = 36, selfCastle = 20), 3 ) // CASTLE
        cards.add( Card("FORTIFY",    CardType.CONSTRUCTION, world, "+12 CASTLE",                 assetManager.getDrawable(CastleGame.IC_FORTIFY), cost = 20, selfCastle = 12), 3 ) // FORTIFY
        cards.add( Card("GREAT WALL",  CardType.CONSTRUCTION, world, "+18 WALL",                  assetManager.getDrawable(CastleGame.IC_GREAT_WALL), cost = 30, selfWall = 18), 2 ) // GREAT WALL
        cards.add( Card("REINFORCE",  CardType.CONSTRUCTION, world, "+6 CASTLE",                  assetManager.getDrawable(CastleGame.IC_REINFORCE), cost = 10, selfCastle = 6), 3 ) // REINFORCE
        cards.add( Card("RESERVE",    CardType.CONSTRUCTION, world, "+8 CASTLE\n-4 WALL",         assetManager.getDrawable(CastleGame.IC_RESERVE), cost = 8,  selfCastle = 8, selfWall = -4), 1 )  // RESERVE
        cards.add( Card("SABOTAGE",   CardType.CONSTRUCTION, world, "+8 CASTLE\n-4 ENEMY CASTLE", assetManager.getDrawable(CastleGame.IC_SABOTAGE), cost = 20, selfCastle = 8, enemyCastle = 4), 1 ) // SABOTAGE
        cards.add( Card("STRONG HOLD", CardType.CONSTRUCTION, world, "+10 CASTLE\n+10 WALL",      assetManager.getDrawable(CastleGame.IC_STRONGHOLD), cost = 36, selfCastle = 10, selfWall = 10), 1 ) // STRONGHOLD
        cards.add( Card("WALL",       CardType.CONSTRUCTION, world, "+10 WALL",                   assetManager.getDrawable(CastleGame.IC_WALL), cost = 20, selfWall = 10), 2 ) // WALL

        // SPECIAL CARDS
        cards.add( Card("BLACK PLAGUE", CardType.SPECIAL_MAGIC, world, "-1 ALL ENEMY\nPEOPLE", assetManager.getDrawable(CastleGame.IC_BLACK_PLAGUE), cost = 72, enemyArmyMen = 1, enemyBuilders = 1, enemyWizards = 1), 1 ) // BLACK PLAGUE
        cards.add( Card("DUPLICATE",     CardType.SPECIAL_CONSTRUCTION, world, "DOUBLE\nWALL HEIGHT", assetManager.getDrawable(CastleGame.IC_DUPLICATE), cost = 52, commands = {player, _ -> BasicDeck.duplicate(player) }), 1 ) // DUPLICATE
        cards.add( Card("JERICHO",       CardType.SPECIAL_WEAPON, world, "DESTROY ENEMY WALL",  assetManager.getDrawable(CastleGame.IC_JERICHO), cost = 64, enemyWall = 100), 1 ) // JERICHO
        cards.add( Card("MERLIN",        CardType.SPECIAL_MAGIC, world, "+32 ATTACK",          assetManager.getDrawable(CastleGame.IC_MERLIN), cost = 64, enemyAttackEither = 32), 1 ) // MERLIN
        cards.add( Card("TROJAN HORSE", CardType.SPECIAL_WEAPON, world, "-20 ENEMY\nCASTLE",    assetManager.getDrawable(CastleGame.IC_TROJAN_HORSE), cost = 56, enemyCastle = 20), 1 ) // TROJAN HORSE

    }

    fun addJapaneseCards() {
        val assetManager = AssetManager.instance

        // WEAPON CARDS
        cards.add( Card("ASHIGARU", CardType.WEAPON, world, "+6 ATTACK",  assetManager.getDrawable(CastleGame.IC_ASHIGARU), cost = 12, enemyAttackEither = 6), 3 ) // ASHIGARU
        cards.add( Card("DOJO",     CardType.WEAPON, world, "+2 SOLDIER", assetManager.getDrawable(CastleGame.IC_DOJO), cost = 32, selfArmyMen = 2), 2 ) // DOJO
        cards.add( Card("KATANA",   CardType.WEAPON, world, "+12 ATTACK", assetManager.getDrawable(CastleGame.IC_KATANA), cost = 22, enemyAttackEither = 12), 2 ) // KATANA
        cards.add( Card("SAMURAI",  CardType.WEAPON, world, "+18 ATTACK", assetManager.getDrawable(CastleGame.IC_SAMURAI), cost = 34, enemyAttackEither = 18), 1 ) // SAMURAI
        cards.add( Card("SHURIKEN", CardType.WEAPON, world, "+10 ATTACK", assetManager.getDrawable(CastleGame.IC_SHURIKEN), cost = 18, enemyAttackEither = 10), 3 ) // SHURIKEN

        // MAGIC CARDS
        cards.add( Card("QUARRY",     CardType.MAGIC, world, "+24 STONE",             assetManager.getDrawable(CastleGame.IC_QUARRY), cost = 14, selfStones = 24), 2 ) // QUARRY
        cards.add( Card("RICE PADDY", CardType.MAGIC, world, "+24 WEAPONS",           assetManager.getDrawable(CastleGame.IC_RICE_PADDY), cost = 14, selfWeapons = 24), 2 ) // RICE PADDY
        cards.add( Card("SEPPUKU",    CardType.MAGIC, world, "-1 SOLDIER\n+2 WIZARD", assetManager.getDrawable(CastleGame.IC_SEPPUKU), cost = 30, selfArmyMen = -1, selfWizards = 2), 1 ) // SEPPUKU
        cards.add( Card("SHRINE",     CardType.MAGIC, world, "+2 WIZARD",             assetManager.getDrawable(CastleGame.IC_SHRINE), cost = 32, selfWizards = 2), 2 ) // SHRINE
        cards.add( Card("TEMPLE",     CardType.MAGIC, world, "+24 GEMS",              assetManager.getDrawable(CastleGame.IC_TEMPLE), cost = 14, selfGems = 24), 2 ) // TEMPLE

        // CONSTRUCTION CARDS
        cards.add( Card("CITADEL",   CardType.CONSTRUCTION, world, "+15 CASTLE",  assetManager.getDrawable(CastleGame.IC_CITADEL), cost = 26, selfCastle = 15), 1 ) // CITADEL
        cards.add( Card("FORTRESS",  CardType.CONSTRUCTION, world, "+8 CASTLE",   assetManager.getDrawable(CastleGame.IC_FORTRESS), cost = 14, selfCastle = 8), 2 ) // FORTRESS
        cards.add( Card("MONASTERY", CardType.CONSTRUCTION, world, "+2 BUILDERS", assetManager.getDrawable(CastleGame.IC_MONASTERY), cost = 32, selfBuilders = 2), 2 ) // MONASTERY
        cards.add( Card("RAMPART",   CardType.CONSTRUCTION, world, "+15 WALL",    assetManager.getDrawable(CastleGame.IC_RAMPART), cost = 26, selfWall = 15), 1 ) // RAMPART
        cards.add( Card("STOCKADE",  CardType.CONSTRUCTION, world, "+8 WALL",     assetManager.getDrawable(CastleGame.IC_STOCKADE), cost = 14, selfWall = 8), 3 ) // STOCKADE

        // SPECIAL CARDS
        cards.add( Card("DRAGON", CardType.SPECIAL_MAGIC, world, "+40 ATTACK",         assetManager.getDrawable(CastleGame.IC_DRAGON), cost = 74, enemyAttackEither = 40), 1 ) // DRAGON
        cards.add( Card("NINJA",  CardType.SPECIAL_WEAPON, world, "-3 ENEMY\nWIZARDS",  assetManager.getDrawable(CastleGame.IC_NINJA), cost = 64, enemyWizards = 3), 1 ) // NINJA
        cards.add( Card("SHOGUN", CardType.SPECIAL_MAGIC, world, "+24 EACH\nRESOURCE", assetManager.getDrawable(CastleGame.IC_SHOGUN), cost = 48, selfGems = 24, selfStones = 24, selfWeapons = 24), 1 ) // SHOGUN

    }

    fun addFlameCards() {
        val assetManager = AssetManager.instance

        // WEAPON CARDS
        cards.add(Card("FIRE",         CardType.WEAPON, world, "+6 ATTACK",  assetManager.getDrawable(CastleGame.IC_FIRE), cost = 12, enemyAttackEither = 6), 3) // FIRE
        cards.add(Card("FIRE ARROWS",  CardType.WEAPON, world, "+10 ATTACK", assetManager.getDrawable(CastleGame.IC_FIRE_ARROW), cost = 18, enemyAttackEither = 10), 3) // FIRE ARROWS
        cards.add(Card("FLAME LEGION", CardType.WEAPON, world, "+2 SOLDIER", assetManager.getDrawable(CastleGame.IC_FLAME_LEGION), cost = 32, selfArmyMen = 2), 2) // FLAME LEGION
        cards.add(Card("FLAMING AXE",  CardType.WEAPON, world, "+12 ATTACK", assetManager.getDrawable(CastleGame.IC_FLAMING_AXE), cost = 22, enemyAttackEither = 12), 2) // FLAMING AXE
        cards.add(Card("FLAMING SHOT", CardType.WEAPON, world, "+18 ATTACK", assetManager.getDrawable(CastleGame.IC_FLAMING_SHOT), cost = 34, enemyAttackEither = 18), 1) // FLAMING SHOT

        // MAGIC CARDS
        cards.add(Card("BLACK SMITH",  CardType.MAGIC, world, "+24 WEAPONS", assetManager.getDrawable(CastleGame.IC_BLACKSMITH), cost = 14, selfWeapons = 24), 2) // BLACKSMITH
        cards.add(Card("COAL",        CardType.MAGIC, world, "+24 GEMS",    assetManager.getDrawable(CastleGame.IC_COAL), cost = 14, selfGems = 24), 2) // COAL
        cards.add(Card("FIREBALL",    CardType.MAGIC, world, "+16 ATTACK",  assetManager.getDrawable(CastleGame.IC_FIREBALL), cost = 30, enemyAttackEither = 16), 1) // FIREBALL
        cards.add(Card("FIRE SHAMAN", CardType.MAGIC, world, "+2 WIZARDS",  assetManager.getDrawable(CastleGame.IC_SHAMAN), cost = 32, selfWizards = 2), 2) // FIRE SHAMAN
        cards.add(Card("LAVA FLOW",   CardType.MAGIC, world, "+24 STONE",   assetManager.getDrawable(CastleGame.IC_LAVA_FLOW), cost = 14, selfStones = 24), 2) // LAVA FLOW

        // CONSTRUCTION CARDS
        cards.add(Card("BOILING OIL",  CardType.CONSTRUCTION, world, "+15 WALL",    assetManager.getDrawable(CastleGame.IC_BOILING_OIL), cost = 26, selfWall = 15), 1) // BOILING OIL
        cards.add(Card("BONFIRE",      CardType.CONSTRUCTION, world, "+15 CASTLE",  assetManager.getDrawable(CastleGame.IC_BONFIRE), cost = 26, selfCastle = 15), 1) // BONFIRE
        cards.add(Card("CAMPFIRE",     CardType.CONSTRUCTION, world, "+8 CASTLE",   assetManager.getDrawable(CastleGame.IC_CAMPFIRE), cost = 14, selfCastle = 8), 3) // CAMPFIRE
        cards.add(Card("FORGE",        CardType.CONSTRUCTION, world, "+2 BUILDERS", assetManager.getDrawable(CastleGame.IC_FORGE), cost = 32, selfBuilders = 2), 2) // FORGE
        cards.add(Card("WALL OF FIRE", CardType.CONSTRUCTION, world, "+8 WALL",     assetManager.getDrawable(CastleGame.IC_WALL_OF_FIRE), cost = 14, selfWall = 8), 3) // WALL OF FIRE

        // SPECIAL CARDS
        cards.add(Card("INFERNO", CardType.SPECIAL_MAGIC, world, "-24 EACH\nENEMY RESOURCE",     assetManager.getDrawable(CastleGame.IC_SWORD), cost = 56, enemyGems = 24, enemyStones = 24, enemyWeapons = 24), 1) // INFERNO
        cards.add(Card("METEORS", CardType.SPECIAL_MAGIC, world, "+40 ATTACK",                   assetManager.getDrawable(CastleGame.IC_METEORS), cost = 74, enemyAttackEither = 40), 1) // METEORS
        cards.add(Card("PHEONIX", CardType.SPECIAL_WEAPON, world, "-1 ENEMY SOLDIER\n+2 SOLDIER", assetManager.getDrawable(CastleGame.IC_PHOENIX), cost = 64, enemyArmyMen = 1, selfArmyMen = 2), 1) // PHEONIX

    }

    fun reload() {
        cards.clear()
        discardPile.clear()
        addDefaultCards()
        if (Settings.flamePackPurchased)
            addFlameCards()
        if (Settings.japanesePackPurchased)
            addJapaneseCards()
    }

}