package com.desitum.castlewars.model

/**
 * Created by kodyvanry on 8/24/17
 */
enum class Tutorial(val info: String) {
    CARD_TUTORIAL("The cost of each card is on the bottom of the card. The cost will be taken from it's associated resource."),
    GARBAGE_CAN_TUTORIAL("Drag and drop cards here to remove them from your deck. This will cost a turn."),
    DRAG_AND_DROP_TUTORIAL("Drag cards around to organize them in your hand. Or long press and release to see what they do!"),
    RESOURCES_TUTORIAL("Each turn resources are given to you. You get 2 of each resource for each townsfolk you have.")
}