package com.desitum.castlewars.model

/**
 * Created by kodyvanry on 7/31/17.
 */
object CardUtilities {

    fun raid(player: Player, enemy: Player) {
        when ((Math.floor(Math.random() * 3)).toInt()) {
            0 -> {
                player.weapons += Math.min(enemy.weapons, 24)
                enemy.weapons = Math.max(enemy.weapons - 24, 0)
            }
            1 -> {
                player.gems += Math.min(enemy.gems, 24)
                enemy.gems = Math.max(enemy.gems - 24, 0)
            }
            else -> {
                player.stones += Math.min(enemy.stones, 24)
                enemy.stones = Math.max(enemy.stones - 24, 0)
            }
        }
    }
}