package com.desitum.castlewars.model

import com.desitum.castlewars.GameScreen
import com.desitum.castlewars.view.Card

/**
 * Created by kodyvanry on 7/21/17.
 */
object AI {
    @JvmStatic fun getCard(aiPlayer: Player, enemyPlayer: Player) : Card {
        if (GameScreen.gameMode == GameScreen.Companion.GameMode.EASY) {
            aiPlayer.hand.forEach {
                if (aiPlayer.canAfford(it))
                    return it
            }
            return aiPlayer.hand[0]
        }
        else {
            val needs = getNeeds(aiPlayer, enemyPlayer)
            var card = aiPlayer.hand[0]
            var currentValue = 0
            aiPlayer.hand.forEach {
                if ((it.flags and needs) > currentValue)
                    card = it
            }
            return card
        }
    }

    @JvmStatic fun getNeeds(aiPlayer: Player, enemyPlayer: Player) : Int {
        var needs = 0
        if (aiPlayer.castle.castleLife > 80 || aiPlayer.castle.castleLife < 20)
            needs = needs or Card.SELF_HEALTH
        if (aiPlayer.wizards + aiPlayer.armyMen + aiPlayer.builders < 6)
            needs = needs or Card.SELF_PEOPLE
        if (aiPlayer.gems + aiPlayer.weapons + aiPlayer.stones < 100)
            needs = needs or Card.SELF_PEOPLE
        if (aiPlayer.gems < 30)
            needs = needs or Card.SELF_GEMS
        if (aiPlayer.weapons < 30)
            needs = needs or Card.SELF_WEAPONS
        if (aiPlayer.stones < 30)
            needs = needs or Card.SELF_STONES

        if (enemyPlayer.castle.castleLife > 80 || enemyPlayer.castle.castleLife < 20)
            needs = needs or Card.ENEMY_HEALTH
        if (enemyPlayer.wizards + enemyPlayer.armyMen + enemyPlayer.builders > 6)
            needs = needs or Card.ENEMY_PEOPLE
        if (enemyPlayer.gems + enemyPlayer.weapons + enemyPlayer.stones > 100)
            needs = needs or Card.ENEMY_RESOURCES

        return needs
    }
}