package com.desitum.castlewars.model

import com.badlogic.gdx.graphics.Color
import com.desitum.castlewars.GameScreen
import com.desitum.castlewars.noneAvailable
import com.desitum.castlewars.persistence.Settings
import com.desitum.castlewars.view.Card
import com.desitum.castlewars.view.Castle
import com.desitum.library.animation.MovementAnimator
import com.desitum.library.game.World
import com.desitum.library.game_objects.Visibility
import com.desitum.library.interpolation.Interpolation
import com.desitum.library.math.CollisionDetection
import com.desitum.library.view.TouchEvent

/**
 * Created by kodyvanry on 6/11/17
 */

class Player(val gameScreen: GameScreen, var useAI: Boolean = false, val castle: Castle) {

    var builders = 2
        set(value) {
            field = Math.max(value, 0)
        }
    var wizards = 2
        set(value) {
            field = Math.max(value, 0)
        }
    var armyMen = 2
        set(value) {
            field = Math.max(value, 0)
        }

    var stones = 10
        set(value) {
            field = Math.max(value, 0)
        }
    var gems = 10
        set(value) {
            field = Math.max(value, 0)
        }
    var weapons = 10
        set(value) {
            field = Math.max(value, 0)
        }
    var enemy: Player? = null
    val world: World = gameScreen.world

    val CARD_START_X: Float
        get() = gameScreen.foregroundViewportWidth / 2 - Card.WIDTH * (Settings.HAND_SIZE.toFloat() / 2f) - Card.SPACING * (Settings.HAND_SIZE.toFloat() / 2f - 0.5f)

    val DEFAULT_PEOPLE : Int
        get() = if (GameScreen.gameMode == GameScreen.Companion.GameMode.HARD && useAI) 3 else 2

    var hand: ArrayList<Card> = ArrayList()
        private set

    var handOnScreen = false

    var playDelay: Float = AI_TURN_TIME

    fun addCardToHand(card: Card) {
        hand.add(card)
        card.setPosition(0f, -card.height)
        if (!useAI) {
            world.addGameObject(card)
            card.visibility = Visibility.VISIBLE
        }
        card.onTouch = { _card, _touch -> onTouch(_card, _touch) }
        card.onTouchUp = { onTouchUp(it) }
        card.onLongPress = { onLongPress(it) }
        card.onClick = { onClick(it) }
    }

    fun update(delta: Float) {
        if (useAI && gameScreen.currentPlayerTurn == this && !gameScreen.gameOver) {
            playDelay -= delta
            if (playDelay <= 0) {
                playDelay = AI_TURN_TIME
                val card = AI.getCard(this, enemy!!)
                if (canAfford(card))
                    gameScreen.useCard(card)
                card.removeCallbacks()
                gameScreen.deck.discard(card)
                hand.remove(card)
                addCardToHand(gameScreen.deck.drawCard())
                gameScreen.endTurn()
            }
        }
        updateCardPosition()
    }

    fun updateForTurn() {
        this.weapons += this.armyMen * 2
        this.gems += this.wizards * 2
        this.stones += this.builders * 2

        if (hand.noneAvailable(this)) {
            hand.forEach {
                it.showTrashCan = true
            }
        } else {
            hand.forEach {
                it.showTrashCan = false
            }
        }

        hand.forEach {
            when (it.cardType) {
                CardType.CONSTRUCTION, CardType.SPECIAL_CONSTRUCTION -> it.enabled = it.cost <= this.stones
                CardType.MAGIC, CardType.SPECIAL_MAGIC -> it.enabled = it.cost <= this.gems
                CardType.WEAPON, CardType.SPECIAL_WEAPON -> it.enabled = it.cost <= this.weapons
            }
        }
    }

    fun useCard(card: Card, enemy: Boolean) {
        if (enemy) {
            stones -= card.enemyStones
            gems -= card.enemyGems
            weapons -= card.enemyWeapons

            builders -= card.enemyBuilders
            wizards -= card.enemyWizards
            armyMen -= card.enemyArmyMen

            val attackWall = card.enemyWall + (card.enemyAttackEither)
            val attackCastle = card.enemyCastle + (Math.max(0, card.enemyAttackEither - castle.wallLife))
            castle.wallLife -= attackWall
            castle.castleLife -= attackCastle
        } else {
            when (card.cardType) {
                CardType.MAGIC, CardType.SPECIAL_MAGIC -> gems -= card.cost
                CardType.WEAPON, CardType.SPECIAL_WEAPON -> weapons -= card.cost
                CardType.CONSTRUCTION, CardType.SPECIAL_CONSTRUCTION -> stones -= card.cost
            }

            stones += card.selfStones
            gems += card.selfGems
            weapons += card.selfWeapons

            builders += card.selfBuilders
            wizards += card.selfWizards
            armyMen += card.selfArmyMen
            castle.wallLife += card.selfWall
            castle.castleLife += card.selfCastle
        }
    }

    fun animateHandOnScreen() {
        while (hand.size < Settings.HAND_SIZE) {
            addCardToHand(gameScreen.deck.drawCard())
        }
        if (!handOnScreen) {
            hand.forEachIndexed { index, card ->
                card.animators.clear()
                card.visibility = Visibility.VISIBLE
                card.color = Color(card.color.r, card.color.g, card.color.b, 1f)
                card.setPosition(
                        CARD_START_X + (index * card.width) + (index * Card.SPACING),
                        0f - card.height
                )
                card.addAndStartAnimator(MovementAnimator(
                        card,
                        0.15f, index * 0.025f,
                        CARD_START_X + (index * card.width) + (index * Card.SPACING),
                        CARD_START_X + (index * card.width) + (index * Card.SPACING),
                        card.y, Card.CARD_Y,
                        Interpolation.DECELERATE_INTERPOLATOR
                ))
            }
        }
        handOnScreen = true
    }

    fun animateHandOffScreen() {
        hand.forEachIndexed { index, card ->
            card.addAndStartAnimator(MovementAnimator(
                    card,
                    0.15f, index * 0.025f,
                    card.x,
                    card.x,
                    card.y, 0f - card.height,
                    Interpolation.DECELERATE_INTERPOLATOR
            ))
        }
        handOnScreen = false
    }


    /**
     * Moves the cards around based on where they need to be.
     *
     * This is a fun UI element that adds character to the game.
     */
    private fun updateCardPosition() {
        if (!hand.contains(world.gameObjectFocus as? Card))
            return
        (world.gameObjectFocus as? Card)?.let { gameObject ->
            var move: Int = 0

            hand.forEachIndexed { index, card ->
                if (index < hand.indexOf(world.gameObjectFocus as? Card) &&
                        (gameObject.x + gameObject.width / 2) < (card.x + card.width / 2)) {
                    move -= 1
                } else if (index > hand.indexOf(world.gameObjectFocus as? Card) &&
                        (gameObject.x + gameObject.width / 2) > (card.x + card.width / 2)) {
                    move += 1
                }
            }

            if (move != 0) { // Move the highlighted game object to the left
                val moveTo = hand.indexOf(gameObject) + move

                hand.remove(gameObject)
                hand.add(moveTo, gameObject)

                hand.forEachIndexed { index, card ->
                    val startPos = card.x
                    val endPos = CARD_START_X + index * card.width + index * Card.SPACING
                    if (startPos != endPos && card != gameObject) {
                        card.addAndStartAnimator(MovementAnimator(card,
                                0.1f, 0f,
                                startPos, endPos, card.y, card.y,
                                Interpolation.ACCELERATE_DECELERATE_INTERPOLATOR
                        ))
                    }
                }
            }
        }
    }

    fun onTouchUp(card: Card) {
        if (CollisionDetection.gameObjectsTouching(card, gameScreen.garbageCan)) {
            throwCardAway(card)
        }
        card.addAndStartAnimator(
                MovementAnimator(
                        card,
                        0.15f, 0f,
                        card.x,
                        CARD_START_X + hand.indexOf(card) * card.width + hand.indexOf(card) * Card.SPACING,
                        card.y, Card.CARD_Y,
                        Interpolation.ACCELERATE_DECELERATE_INTERPOLATOR
                )
        )
        gameScreen.garbageCan.close()
    }

    fun throwCardAway(card: Card) {
        card.removeCallbacks()
        gameScreen.deck.discard(card)
        gameScreen.world.removeGameObject(card)
        hand.remove(card)
        gameScreen.endTurn()
    }

    fun onLongPress(card: Card) {
        gameScreen.showDescription(card)
    }

    fun onClick(card: Card) {
        if (card.showTrashCan) {
            throwCardAway(card)
        } else {
            gameScreen.useCard(card)
            card.removeCallbacks()
            gameScreen.deck.discard(card)
            gameScreen.world.removeGameObject(card)
            hand.remove(card)
            gameScreen.endTurn()
        }
    }

    fun onTouch(card: Card, touchEvent: TouchEvent) {
        if (CollisionDetection.gameObjectsTouching(card, gameScreen.garbageCan)) {
            gameScreen.garbageCan.open()
        } else {
            gameScreen.garbageCan.close()
        }
        gameScreen.dismissDescription()
    }

    fun canAfford(card: Card): Boolean {
        when (card.cardType) {
            CardType.WEAPON, CardType.SPECIAL_WEAPON -> return weapons >= card.cost
            CardType.CONSTRUCTION, CardType.SPECIAL_CONSTRUCTION -> return stones >= card.cost
            CardType.MAGIC, CardType.SPECIAL_MAGIC -> return gems >= card.cost
        }
    }

    companion object {
        val AI_TURN_TIME = 2.5f
        val DEFAULT_RESOURCES = 10
    }

    fun reset() {
        armyMen = DEFAULT_PEOPLE
        wizards = DEFAULT_PEOPLE
        builders = DEFAULT_PEOPLE
        weapons = DEFAULT_RESOURCES
        gems = DEFAULT_RESOURCES
        stones = DEFAULT_RESOURCES
        handOnScreen = false
    }

    fun emptyHand() {
        hand.forEach {
            world.removeGameObject(it)
        }
        hand.clear()
    }
}