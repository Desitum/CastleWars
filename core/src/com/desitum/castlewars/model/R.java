package com.desitum.castlewars.model;

import org.jetbrains.annotations.NotNull;

/**
 * Created by kodyvanry on 7/5/17.
 */

public class R {
    public class id {
        public static final int play_icon =                 0x7f010001;
        public static final int button_easy =               0x7f010011;
        public static final int button_hard =               0x7f010012;
        public static final int play_menu =                 0x7f010013;
        public static final int shop_menu =                 0x7f010014;
        public static final int settings_menu =             0x7f010015;
        public static final int p_1_army_inventory =        0x7f010016;
        public static final int p_1_gem_inventory =         0x7f010017;
        public static final int p_1_builder_inventory =     0x7f010018;
        public static final int p_2_army_inventory =        0x7f010019;
        public static final int p_2_gem_inventory =         0x7f01001a;
        public static final int p_2_builder_inventory =     0x7f01001b;
        public static final int castle_wars_sign =          0x7f01001c;
        public static final int button_volume =             0x7f01001d;
        public static final int settings_menu_in_game =     0x7f01001e;
        public static final int button_home =               0x7f01001f;
        public static final int theme_default =             0x7f010020;
        public static final int theme_flame =               0x7f010021;
        public static final int theme_japanese =            0x7f010022;
        public static final int shopping_flame =            0x7f010023;
        public static final int shopping_japanese =         0x7f010024;
        public static final int shopping_card_1 =           0x7f010025;
        public static final int shopping_card_2 =           0x7f010026;
        public static final int button_pass_and_play =      0x7f010027;
    }

    public class drawable {
        public static final int play_icon =                 0x7f010001;
        public static final int button_easy =               0x7f010011;
        public static final int button_hard =               0x7f010012;
    }
}
