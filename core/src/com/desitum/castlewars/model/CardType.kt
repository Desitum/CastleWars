package com.desitum.castlewars.model

import com.badlogic.gdx.graphics.Color
import com.desitum.castlewars.CastleGame

/**
 * Created by kodyvanry on 6/8/17.
 */
enum class CardType(val color: Color) {
    WEAPON(CastleGame.RED),
    MAGIC(CastleGame.GREEN),
    CONSTRUCTION(CastleGame.BLUE),
    SPECIAL_WEAPON(CastleGame.RED),
    SPECIAL_MAGIC(CastleGame.GREEN),
    SPECIAL_CONSTRUCTION(CastleGame.BLUE)
}