package com.desitum.castlewars

import com.desitum.castlewars.model.Player
import java.util.*

/**
 * Created by kodyvanry on 8/2/17
 */

object BasicDeck {

    fun raid(enemy: Player) {
        when (Random().randInt(0, 3)) {
            0 -> enemy.stones -= 24
            1 -> enemy.gems -= 24
            else -> enemy.weapons -= 24
        }
    }

    fun assassin(enemy: Player) {
        when (Random().randInt(0, 2)) {
            0 -> enemy.armyMen -= 1;
            1 -> enemy.wizards -= 1;
            else -> enemy.builders -= 1;
        }
    }

    fun duplicate(player: Player) {
        player.castle.wallLife *= 2
    }

}