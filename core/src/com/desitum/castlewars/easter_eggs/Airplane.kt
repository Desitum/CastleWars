package com.desitum.castlewars.easter_eggs

import com.desitum.castlewars.CastleGame
import com.desitum.library.animation.MovementAnimator
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.game_objects.GameObject
import com.desitum.library.interpolation.Interpolation

/**
 * Created by kodyvanry on 8/24/17
 */
class Airplane(world: World) : GameObject(AssetManager.instance.getDrawable(CastleGame.AIRPLANE).textureRegion, world) {

    init {
        z = 10000
        setSize(200f, 90f)
        setPosition(0f, 300f)
    }

    override fun update(delta: Float) {
        super.update(delta)
        speed = 1f
        moveToPosition(500f, 500f)
    }

}