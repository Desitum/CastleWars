package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.CastleGame
import com.desitum.castlewars.GameScreen
import com.desitum.castlewars.clone
import com.desitum.castlewars.fadeOut
import com.desitum.library.drawing.Drawable
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.logging.Log
import com.desitum.library.view.*

/**
 * Created by kodyvanry on 7/31/17
 */
class ConfirmationView constructor(gameScreen: GameScreen, layoutConstraints: LayoutConstraints, font: BitmapFont, background: Drawable, var text: String, val onYes: () -> Unit = {}, val onNo: () -> Unit = {} ) : ViewGroup(gameScreen.world, layoutConstraints), View.OnClickListener {

    var alignment: Int = 0
    private val glyphLayout: GlyphLayout = GlyphLayout(font, text)
    var textColor: Color
    val yesText: TextView
    val noText: TextView
    val font: BitmapFont = font.clone()
    val disableView = View(world, LayoutConstraints(0f, 0f, gameScreen.foregroundViewportWidth, gameScreen.foregroundViewportHeight))

    init {
        val world = gameScreen.world
        gameScreen.world.addView(disableView)
        disableView.clickable = true
        disableView.onClickListener = this
        glyphLayout.setText(font, text, Color.BLACK, textWidth, Align.center, true)
        textColor = Color.BLACK
        this.alignment = LinearLayout.ALIGNMENT_CENTER
        backgroundDrawable = background

        yesText = TextView(world, LayoutConstraints(
              0f, 0f, layoutConstraints.width / 2f, 100f
        ), font.clone())
        yesText.id = YES
        yesText.text = "YES"
        yesText.textColor = CastleGame.GREEN
        yesText.clickable = true
        yesText.alignment = Align.center
//        yesText.backgroundDrawable = AssetManager.instance.getDrawable(CastleGame.BLACK_CARD)
        yesText.onClickListener = this

        noText = TextView(world, LayoutConstraints(
              layoutConstraints.width / 2f, 0f, layoutConstraints.width / 2f, 100f
        ), font.clone())
        noText.id = NO
        noText.text = "NO"
        noText.textColor = CastleGame.RED
        noText.clickable = true
        noText.alignment = Align.center
        noText.onClickListener = this
        z = 50

        addView(yesText)
        addView(noText)
    }

    override fun onClick(view: View) {
        when (view.id) {
            NO -> {
                onNo()
                world?.removeView(disableView)
                clearAnimators()
                fadeOut()
            }
            YES -> {
                onYes()
                world?.removeView(disableView)
                clearAnimators()
                fadeOut()
            }
        }
    }

    override fun onDraw(batch: Batch, viewport: Viewport) {
        super.onDraw(batch, viewport)
        font.draw(batch, glyphLayout, x + width / 2 - textWidth / 2, y + height / 2 + textHeight / 2f)
    }

    val textWidth: Float
        get() {
            return layoutConstraints.width * .8f
        }

    var _textHeight: Float? = null
    val textHeight: Float
        get() {
            _textHeight?.let { return it }
            glyphLayout.setText(font, text, Color.BLACK, textWidth, Align.center, true)
            _textHeight = glyphLayout.height
            return _textHeight!!
        }

    companion object {
        val ARROW_SIZE = 50f
        val NO = 0
        val YES = 1
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
//        Log.d(this::class.java, "setColor 1")
        font?.setColor(textColor.r, textColor.g, textColor.b, a)
//        glyphLayout?.setText(font, text, Color(0f, 0f, 0f, a), textWidth, Align.center, true)
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
//        Log.d(this::class.java, "setColor 2")
//        font?.setColor(textColor.r, textColor.g, textColor.b, tint.a)
        glyphLayout?.setText(font, text, Color(0f, 0f, 0f, tint.a), textWidth, Align.center, true)
    }
}