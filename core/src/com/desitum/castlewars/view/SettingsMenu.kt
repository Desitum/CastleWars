package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.desitum.castlewars.CastleGame
import com.desitum.castlewars.fadeIn
import com.desitum.castlewars.fadeOut
import com.desitum.library.animation.ColorEffects
import com.desitum.library.animation.SizeAnimator
import com.desitum.library.drawing.Drawable
import com.desitum.library.game.World
import com.desitum.library.game_objects.Visibility
import com.desitum.library.interpolation.Interpolation
import com.desitum.library.logging.Log
import com.desitum.library.view.*

/**
 * Created by kodyvanry on 8/10/17.
 */
class SettingsMenu(world: World, val buttonLayoutConstraints: LayoutConstraints, val menuLayoutConstraints: LayoutConstraints, val iconDrawable: Drawable) : ViewGroup(world, buttonLayoutConstraints) {

    enum class State {
        BUTTON,
        MENU
    }

    val Z = 32f

    val TRANSITION_TIME = .25f

    val ICON_WIDTH: Float
        get() = buttonLayoutConstraints.width / 2f
    val ICON_HEIGHT: Float
        get() = buttonLayoutConstraints.width / 2f

    var state = State.BUTTON

    override var visibility: Visibility = Visibility.VISIBLE
        get() = field
        set(value) {
            field = value
            imageView.visibility = value
        }

    val viewLayoutConstraintsButton = LayoutConstraints(
            0f, 0f, ICON_WIDTH, ICON_HEIGHT,
            marginStart = buttonLayoutConstraints.width / 2f - ICON_WIDTH / 2f,
            marginBottom = buttonLayoutConstraints.height / 2f - ICON_HEIGHT / 2f,
            alignParentStart = true, alignParentBottom = true)

    var imageView = View(world, viewLayoutConstraintsButton)

    init {
        z = 40
        imageView.backgroundDrawable = iconDrawable
        imageView.color = CastleGame.GREY
        imageView.onClick = {
            if (state == State.BUTTON) {
                transitionToMenu()
            } else {
                transitionToButton()
            }
        }
        imageView.clickable = true

        super.addView(imageView)
    }

    private fun transitionToMenu() {
        clearAnimators()
        val heightAnimator = SizeAnimator(this, TRANSITION_TIME, 0f, width, width, height, menuLayoutConstraints.height, Interpolation.ACCELERATE_DECELERATE_INTERPOLATOR)

        children.forEachIndexed { index, view ->
            if (view != imageView) {
                view.clearAnimators()
                view.fadeIn(index * TRANSITION_TIME / 3f)
            }
        }

        startAnimator(heightAnimator)

        state = State.MENU
    }

    fun transitionToButton() {
        clearAnimators()
        val heightAnimator = SizeAnimator(this, TRANSITION_TIME, 0f, width, width, height, Card.HEIGHT, Interpolation.ACCELERATE_DECELERATE_INTERPOLATOR)

        startAnimator(heightAnimator)

        children.forEachIndexed { index, view ->
            if (view != imageView) {
                view.clearAnimators()
                view.fadeOut()
            }
        }

        state = State.BUTTON
    }

    override fun addView(v: View) {
        super.addView(v)
        v.color = Color(v.color.r, v.color.g, v.color.b, 0f)
        v.visibility = Visibility.GONE
    }

    override fun setColor(color: Float) {
        super.setColor(color)
//        children.forEach { it.setColor(it.color.r, it.color.g, it.color.b, Color.) }
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
        imageView?.color = Color(imageView.color.r, imageView.color.g, imageView.color.b, tint.a)
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        imageView?.setColor(imageView.color.r, imageView.color.g, imageView.color.b, a)
    }
}
