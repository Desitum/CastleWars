package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.CastleGame
import com.desitum.castlewars.GameScreen
import com.desitum.castlewars.chooseRandom
import com.desitum.castlewars.randFloat
import com.desitum.library.drawing.Drawable
import com.desitum.library.game.AssetManager
import com.desitum.library.game_objects.GameObject
import java.util.*

/**
 * Created by kodyvanry on 8/14/17
 */
class Clouds(val gameScreen: GameScreen) : GameObject(null, gameScreen.world) {

    private val cloudList = ArrayList<Cloud>()
    private var nextCloud = 0f

    override fun update(delta: Float) {
        super.update(delta)
        cloudList.forEach {
            it.update(delta)
        }
        if (nextCloud <= 0f) {
            cloudList.add(AssetManager.instance.getRandomCloud(gameScreen.viewportWidth, gameScreen.viewportHeight))
            nextCloud = Random().randFloat(10f, 20f)
        }
        nextCloud -= delta
    }

    override fun onDraw(spriteBatch: Batch, viewport: Viewport) {
        super.onDraw(spriteBatch, viewport)
        cloudList.forEach {
            it.draw(spriteBatch)
        }
    }

}

class Cloud(val drawable: Drawable, var x: Float, val y: Float, val speed: Float) {

    val width = drawable.width
    val height = drawable.height


    fun update(delta: Float) {
        x += speed * delta
    }

    fun draw(batch: Batch) {
        drawable.draw(batch, x, y, width, height)
    }
}

fun AssetManager.getRandomCloud(screenWidth: Float, screenHeight: Float) : Cloud {
    val randomDrawable = getDrawable(arrayOf(CastleGame.CLOUD_1, CastleGame.CLOUD_2, CastleGame.CLOUD_3).chooseRandom())
    return Cloud(
            randomDrawable,
            0f - randomDrawable.width,
            Random().randFloat(screenHeight / 2f, screenHeight - randomDrawable.height),
            Random().randFloat(screenWidth / 32, screenWidth / 16)
    )
}