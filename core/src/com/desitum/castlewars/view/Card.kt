package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.CastleGame
import com.desitum.castlewars.clone
import com.desitum.castlewars.model.CardType
import com.desitum.castlewars.model.Player
import com.desitum.library.drawing.Drawable
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.game_objects.GameObject
import com.desitum.library.view.LinearLayout
import com.desitum.library.view.TouchEvent

/**
 * Created by kodyvanry on 6/8/17
 */


// Don't *LOVE* this, but I do know personally that it's textureRegion isn't null
class Card(val name: String,
           val cardType: CardType, world: World?, val description: String,
           val imageDrawable: Drawable,
           val enemyStones: Int = 0, val enemyGems: Int = 0, val enemyWeapons: Int = 0,
           val enemyBuilders: Int = 0, val enemyWizards: Int = 0, val enemyArmyMen: Int = 0,
           val enemyWall: Int = 0, val enemyCastle: Int = 0, val enemyAttackEither: Int = 0,
           val selfStones: Int = 0, val selfGems: Int = 0, val selfWeapons: Int = 0,
           val selfBuilders: Int = 0, val selfWizards: Int = 0, val selfArmyMen: Int = 0,
           val selfWall: Int = 0, val selfCastle: Int = 0,
           val commands: (self: Player, enemy: Player) -> Unit = { _, _ -> },
           val cost: Int = 0,
           flags: Int = 0
) : GameObject(null, world) {

    private var lastTouchEvent: TouchEvent = TouchEvent()
    private var originalTouchEvent: TouchEvent = TouchEvent()
    private var myWorld: World? = null
    var dragged = false
    var onTouchUp: (Card) -> Unit = {}
    var onTouch: (Card, TouchEvent) -> Unit = { _, _ -> }
    var onDragged: (Card) -> Unit = {}
    var onLongPress: (Card) -> Unit = {}
    var onClick: (Card) -> Unit = {}
    private var startTouch = 0L
    private val _world: World?
    var enabled = true
    var showTrashCan = true
    private val disabledDrawable = AssetManager.instance.getDrawable(CastleGame.DISABLED_CARD)
    private val deleteDrawable = AssetManager.instance.getDrawable(CastleGame.IC_TRASH)
    val glyphLayout: GlyphLayout = GlyphLayout(font, name)
    val costGlyphLayout: GlyphLayout = GlyphLayout(costFont, "$cost")
    val flags: Int = flags
        get() {
            var flag = 0
            if (selfWall != 0 || selfCastle != 0) flag = field or Card.SELF_HEALTH
            if (selfArmyMen > 0 || selfWizards > 0 || selfBuilders > 0) flag = field or Card.SELF_PEOPLE
            if (selfGems > 0) flag = field or Card.SELF_GEMS
            if (selfStones > 0) flag = field or Card.SELF_STONES
            if (selfWeapons > 0) flag = field or Card.SELF_WEAPONS
            if (enemyArmyMen != 0 || enemyWizards != 0 || enemyBuilders != 0) flag = field or Card.ENEMY_PEOPLE
            if (enemyGems != 0 || enemyStones != 0 || enemyWeapons != 0) flag = field or Card.ENEMY_RESOURCES
            if (enemyWall != 0 || enemyCastle != 0) flag = field or Card.ENEMY_HEALTH
            return flag
        }

    init {
        setSize(150f, 225f)
        _world = world
        myWorld = world
        focusable = true
        drawable = if (cardType in arrayOf(CardType.SPECIAL_WEAPON, CardType.SPECIAL_MAGIC, CardType.SPECIAL_CONSTRUCTION)) AssetManager.instance.getDrawable(CastleGame.BLACK_CARD) else AssetManager.instance.getDrawable(CastleGame.CARD)
        drawable!!.color = cardType.color
//        font.data.scaleX = 4f
//        font.scaleX = 3f
        font.data.setScale(0.3f)
        costFont.data.setScale(0.4f)
        glyphLayout.setText(font, name, Color.WHITE, textWidth, Align.center, true)
        costGlyphLayout.setText(costFont, "$cost", Color.WHITE, textWidth, Align.center, true)
    }

    override fun onDraw(spriteBatch: Batch, viewport: Viewport) {
        super.onDraw(spriteBatch, viewport)

        imageDrawable.draw(spriteBatch, x + width / 4, y + height / 4, width / 2, width / 2)
        font.draw(spriteBatch, glyphLayout, x + width / 2 - textWidth / 2, y + height * 0.8f)
        costFont.draw(spriteBatch, costGlyphLayout, x + width / 2 - textWidth / 2, y + height * 0.2f)

        if (!enabled) {
            disabledDrawable.draw(spriteBatch, x, y, width, height)
            if (showTrashCan) {
                deleteDrawable.draw(spriteBatch, x + width / 2 - deleteDrawable.width / 2,
                        y + height / 2 - deleteDrawable.height / 2,
                        deleteDrawable.width,
                        deleteDrawable.height)
            }
        }
    }

    override fun onTouchEvent(touchEvent: TouchEvent): Boolean {
        onTouch(this, touchEvent)
        when (touchEvent.action) {
            TouchEvent.Action.DOWN -> {
                originalTouchEvent.x = touchEvent.x
                originalTouchEvent.y = touchEvent.y
                lastTouchEvent.x = touchEvent.x
                lastTouchEvent.y = touchEvent.y
                z += 1
                myWorld?.sortGameObjects()
                startTouch = System.currentTimeMillis()
                dragged = false
            }
            TouchEvent.Action.MOVE -> {
                x = x + touchEvent.x - lastTouchEvent.x
                y = y + touchEvent.y - lastTouchEvent.y
                lastTouchEvent.x = touchEvent.x
                lastTouchEvent.y = touchEvent.y
                dragged = (Math.abs(originalTouchEvent.x - touchEvent.x) + Math.abs(originalTouchEvent.y - touchEvent.y) > 20)
            }
            TouchEvent.Action.UP -> {
                z = GameObject.DEFAULT_Z
                myWorld?.sortGameObjects()
                if (dragged) {
                    onDragged(this)
                } else if (System.currentTimeMillis() - startTouch > 250) { // LONG PRESS
                    onLongPress(this)
                } else if (enabled || showTrashCan) {
                    onClick(this)
                }
                onTouchUp(this)
            }
            else -> {
            }

        }
        return super.onTouchEvent(touchEvent)
    }

    fun disable() {
        enabled = false
    }

    fun enable() {
        enabled = true
    }

    fun clone(): Card {
        return Card(name,
                cardType,
                _world,
                description,
                imageDrawable,
                enemyStones,
                enemyGems,
                enemyWeapons,
                enemyBuilders,
                enemyWizards,
                enemyArmyMen,
                enemyWall,
                enemyCastle,
                enemyAttackEither,
                selfStones,
                selfGems,
                selfWeapons,
                selfBuilders,
                selfWizards,
                selfArmyMen,
                selfWall,
                selfCastle,
                commands,
                cost,
                flags)
    }

    fun removeCallbacks() {
        onTouchUp = {}
        onTouch = { _, _ -> }
        onDragged = {}
        onLongPress = {}
        onClick = {}
    }

    val textWidth: Float
        get() {
            return WIDTH * 0.9f
        }

    var _textHeight: Float? = null
    val textHeight: Float
        get() {
            _textHeight?.let { return it }
            glyphLayout.setText(font, name, Color.WHITE, textWidth, Align.center, true)
            _textHeight = glyphLayout.height
            return _textHeight!!
        }

    companion object {
        val CARD_Y = 25f
        val SPACING = 20f
        val WIDTH = 150f
        val HEIGHT = 225f

        val ENEMY_HEALTH: Int = 1
        val ENEMY_RESOURCES: Int = 1 shl 1
        val ENEMY_PEOPLE: Int = 1 shl 2
        val SELF_PEOPLE: Int = 1 shl 3
        val SELF_STONES: Int = 1 shl 4
        val SELF_WEAPONS: Int = 1 shl 5
        val SELF_GEMS: Int = 1 shl 6
        val SELF_HEALTH: Int = 1 shl 7
        val font: BitmapFont = AssetManager.instance.getFont(CastleGame.MAIN_FONT).clone()
        val costFont: BitmapFont = AssetManager.instance.getFont(CastleGame.MAIN_FONT).clone()
    }
}
