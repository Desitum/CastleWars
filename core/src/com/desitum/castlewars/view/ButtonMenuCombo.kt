package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.CastleGame
import com.desitum.castlewars.fadeIn
import com.desitum.castlewars.fadeOut
import com.desitum.library.animation.Animator
import com.desitum.library.animation.ColorEffects
import com.desitum.library.animation.MarginAnimator
import com.desitum.library.animation.SizeAnimator
import com.desitum.library.drawing.Drawable
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.game_objects.Visibility
import com.desitum.library.interpolation.Interpolation
import com.desitum.library.logging.Log
import com.desitum.library.view.*

/**
 * Created by kodyvanry on 7/3/17
 */
class ButtonMenuCombo(world: World, val buttonLayoutConstraints: LayoutConstraints,
                      val menuLayoutConstraints: LayoutConstraints, val iconDrawable: Drawable,
                        val onCloseListener: OnCloseListener) : ViewGroup(world, buttonLayoutConstraints.clone()) {

    enum class State {
        BUTTON,
        MENU
    }

    val Z = 32f

    val TRANSITION_TIME = .25f

    val ICON_WIDTH: Float
        get() = buttonLayoutConstraints.width / 2f
    val ICON_HEIGHT: Float
        get() = buttonLayoutConstraints.width / 2f

    val ICON_SMALL_WIDTH: Float
        get() = buttonLayoutConstraints.width / 3f
    val ICON_SMALL_HEIGHT: Float
        get() = buttonLayoutConstraints.width / 3f

    var state = State.BUTTON

    val viewLayoutConstraintsButton = LayoutConstraints(
            0f, 0f, ICON_WIDTH, ICON_HEIGHT,
            marginStart = buttonLayoutConstraints.width / 2f - ICON_WIDTH / 2f,
            marginTop = buttonLayoutConstraints.height / 2f - ICON_HEIGHT / 2f,
            alignParentStart = true, alignParentTop = true)

    val viewLayoutConstraintsButtonSmall = LayoutConstraints(
            0f, 0f, ICON_SMALL_WIDTH, ICON_SMALL_HEIGHT,
            marginStart = buttonLayoutConstraints.width / 2f - ICON_SMALL_WIDTH / 2f,
            marginTop = buttonLayoutConstraints.width / 2f - ICON_SMALL_HEIGHT / 2f,
            alignParentStart = true, alignParentTop = true)

    val viewLayoutConstraintsClose = LayoutConstraints(
            0f, 0f, ICON_SMALL_WIDTH, ICON_SMALL_HEIGHT,
            marginEnd = buttonLayoutConstraints.width / 2f - ICON_SMALL_WIDTH / 2f,
            marginTop = buttonLayoutConstraints.width / 2f - ICON_SMALL_HEIGHT / 2f,
            alignParentEnd = true, alignParentTop = true)

    var imageView = ImageView(world, viewLayoutConstraintsButton)
    var closeButton = Button(world, viewLayoutConstraintsClose.clone())
//    var shadow: Drawable

    init {
//        val view = ImageView(world, LayoutConstraints(
//                0f, 0f, 0f, 0f,
//                alignParentStart = true, alignParentTop = true, alignParentBottom = true, alignParentEnd = true,
//                marginStart = 2f, marginTop = 2f, marginBottom = 4f, marginEnd = 4f
//        ))
//        view.backgroundDrawable = AssetManager.instance.getDrawable(CastleGame.MENU_CARD)
        imageView.backgroundDrawable = iconDrawable
//        shadow = AssetManager.instance.getDrawable(CastleGame.CARD_SHADOW)
        closeButton.restDrawable = AssetManager.instance.getDrawable(CastleGame.CLOSE_X)
        imageView.color = CastleGame.GREY
        closeButton.color = CastleGame.GREY
        closeButton.visibility = Visibility.GONE
        closeButton.onClick = {
            transitionToButton()
            onCloseListener.onClose(this)
        }

        super.addView(imageView)
        addView(closeButton)
    }

    override fun onTouchEvent(touchEvent: TouchEvent): Boolean {
        if (state == State.BUTTON && touchEvent.action == TouchEvent.Action.UP && isTouching(touchEvent)) {
            transitionToMenu()
            onClickListener?.onClick(this)
        }
        return super.onTouchEvent(touchEvent)
    }

//    override fun draw(batch: Batch, alphaModulation: Float) {
//        shadow.draw(batch, x - Z, y - Z * 2, width + Z * 3, height + Z * 3)
//        shadow.draw(batch, 1f, 1f, 100f, 100f)
//        super.draw(batch, alphaModulation)
//    }

//    override fun draw(batch: Batch, viewport: Viewport) {
////        shadow.draw(batch, x - Z * 1.1f, y - Z * 1.2f, width + Z * 2.3f, height + Z * 2.2f)
////        shadow.draw(batch, 1f, 1f, 100f, 100f)
//        super.draw(batch, viewport)
//    }

    private fun transitionToMenu() {
        clearAnimators()
        val horizontalAnimator = CenterAnimator(this, TRANSITION_TIME, 0f, x + width / 2, menuLayoutConstraints.x + menuLayoutConstraints.width / 2f, 0f, 0f, Interpolation.DECELERATE_INTERPOLATOR)
        val verticalAnimator = CenterAnimator(this, TRANSITION_TIME, 0f, 0f, 0f, y + height / 2, menuLayoutConstraints.y + menuLayoutConstraints.height / 2f, Interpolation.ACCELERATE_INTERPOLATOR)
        val widthAnimator = SizeAnimator(this, TRANSITION_TIME * 0.666f, 0f, width, menuLayoutConstraints.width, 0f, 0f, Interpolation.ACCELERATE_DECELERATE_INTERPOLATOR)
        val heightAnimator = SizeAnimator(this, TRANSITION_TIME, 0f, width, width, height, menuLayoutConstraints.height, Interpolation.ACCELERATE_DECELERATE_INTERPOLATOR)

        val marginAnimator = MarginAnimator(imageView, viewLayoutConstraintsButton, viewLayoutConstraintsButtonSmall, TRANSITION_TIME, Interpolation.LINEAR_INTERPOLATOR, 0f)
        val imageViewSizeAnimator = SizeAnimator(imageView, TRANSITION_TIME, 0f, viewLayoutConstraintsButton.width, viewLayoutConstraintsButtonSmall.width, viewLayoutConstraintsButton.height, viewLayoutConstraintsButtonSmall.height, Interpolation.LINEAR_INTERPOLATOR)
        imageView.startAnimator(marginAnimator)
        imageView.startAnimator(imageViewSizeAnimator)

        children.forEach {
            if (it != imageView) {
                it.clearAnimators()
                it.fadeIn(0.25f)
            }
        }

        startAnimator(widthAnimator)
        startAnimator(heightAnimator)
        startAnimator(horizontalAnimator)
        startAnimator(verticalAnimator)

        state = State.MENU
    }

    fun transitionToButton() {
        if (state != State.BUTTON) {
            clearAnimators()
            val horizontalAnimator = CenterAnimator(this, TRANSITION_TIME, 0f, x + width / 2, buttonLayoutConstraints.x + buttonLayoutConstraints.width / 2f, 0f, 0f, Interpolation.ACCELERATE_INTERPOLATOR)
            val verticalAnimator = CenterAnimator(this, TRANSITION_TIME, 0f, 0f, 0f, y + height / 2, buttonLayoutConstraints.y + buttonLayoutConstraints.height / 2f, Interpolation.DECELERATE_INTERPOLATOR)
            val widthAnimator = SizeAnimator(this, TRANSITION_TIME, 0f, width, Card.WIDTH, 0f, 0f, Interpolation.ACCELERATE_DECELERATE_INTERPOLATOR)
            val heightAnimator = SizeAnimator(this, TRANSITION_TIME * 0.666f, 0f, width, width, height, Card.HEIGHT, Interpolation.ACCELERATE_DECELERATE_INTERPOLATOR)
            startAnimator(widthAnimator)
            startAnimator(heightAnimator)
            startAnimator(horizontalAnimator)
            startAnimator(verticalAnimator)
            val marginAnimator = MarginAnimator(imageView, viewLayoutConstraintsButtonSmall, viewLayoutConstraintsButton, TRANSITION_TIME, Interpolation.LINEAR_INTERPOLATOR, 0f)
            val imageViewSizeAnimator = SizeAnimator(imageView, TRANSITION_TIME, 0f, viewLayoutConstraintsButtonSmall.width, viewLayoutConstraintsButton.width, viewLayoutConstraintsButtonSmall.height, viewLayoutConstraintsButton.height, Interpolation.LINEAR_INTERPOLATOR)
            imageView.startAnimator(marginAnimator)
            imageView.startAnimator(imageViewSizeAnimator)

            children.forEach {
                if (it != imageView) {
                    it.clearAnimators()
                    it.fadeOut(duration = 0.1f)
//                val colorEffects = ColorEffects(it, TRANSITION_TIME / 2f, 0f, it.color, Color(0f, 0f, 0f, 0f))
//                colorEffects.onAnimationFinished = { animator ->
//                    it.visibility = Visibility.GONE
//                }
//                it.startAnimator(colorEffects)
                }
            }

            state = State.BUTTON
        }
    }

    class CenterAnimator(val buttonMenuCombo: ButtonMenuCombo, duration: Float, delay: Float,
                         private val startX: Float, private val endX: Float,
                         private val startY: Float, private val endY: Float,
                         interpolator: Int) :
            Animator(buttonMenuCombo, duration, delay) {

        var distanceX: Float = endX - startX
            private set

        var distanceY: Float = endY - startY
            private set

        init {
            setupInterpolator(interpolator)
        }

        override fun updateAnimation() {
            if (startX != 0f || endX != 0f || distanceX != 0f)
                sprite?.x = interpolator!!.getInterpolation(timeInAnimation) * distanceX + startX - buttonMenuCombo.width / 2f
            if (startY != 0f || endY != 0f || distanceY != 0f)
                sprite?.y = interpolator!!.getInterpolation(timeInAnimation) * distanceY + startY - buttonMenuCombo.height / 2f
        }

        override fun clone(): Animator {
            return CenterAnimator(buttonMenuCombo, duration, delay, startX, endX, startY, endY, Interpolation.getInterpolatorNum(interpolator!!))
        }
    }

    fun reset() {
        state = State.BUTTON
        children.forEach {
            if (it != imageView)
                it.visibility = Visibility.GONE
        }
        imageView.layoutConstraints = viewLayoutConstraintsButton.clone()
        layoutConstraints = buttonLayoutConstraints.clone()
        invalidate()
    }

    class ImageView(world: World, layoutConstraints: LayoutConstraints) : View(world, layoutConstraints) {
        override fun onTouchEvent(touchEvent: TouchEvent): Boolean {
            return false
        }
    }

    override fun addView(v: View) {
        super.addView(v)
        v.color = Color(v.color.r, v.color.g, v.color.b, 0f)
        v.visibility = Visibility.GONE
    }

    override fun setColor(color: Float) {
        super.setColor(color)
//        children.forEach { it.setColor(it.color.r, it.color.g, it.color.b, Color.) }
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
        children?.forEach {
            if (it != imageView)
                it.setColor(it.color.r, it.color.g, it.color.b, 0f)
            else
                it.setColor(it.color.r, it.color.g, it.color.b, tint.a)
        }
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        children?.forEach {
            if (it != imageView)
                it.setColor(it.color.r, it.color.g, it.color.b, 0f)
            else
                it.setColor(it.color.r, it.color.g, it.color.b, a)
        }
    }

    interface OnCloseListener {
        fun onClose(buttonMenuCombo: ButtonMenuCombo)
    }
}
