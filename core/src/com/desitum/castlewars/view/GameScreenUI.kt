package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.desitum.castlewars.CastleGame
import com.desitum.castlewars.GameScreen
import com.desitum.castlewars.model.CardType
import com.desitum.castlewars.model.R
import com.desitum.library.drawing.Drawable
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.game_objects.GameObject
import com.desitum.library.game_objects.Visibility
import com.desitum.library.view.*

/**
 * Created by kodyvanry on 7/2/17
 */

class GameScreenUI {

    companion object {

        @JvmStatic fun setupUI(gameScreen: GameScreen) {
            setupBackground(gameScreen,
                    gameScreen.viewport.worldWidth,
                    gameScreen.viewport.worldHeight,
                    gameScreen.playerCastle, gameScreen.enemyCastle)

            setupCards(
                    gameScreen,
                    gameScreen.world,
                    gameScreen.garbageCan
            )

            setupForeground(
                    gameScreen,
                    gameScreen.foregroundViewportWidth,
                    gameScreen.foregroundViewportHeight
            )

            setupMenus(gameScreen)
        }

        fun setupBackground(gameScreen: GameScreen, viewportWidth: Float, viewportHeight: Float, playerCastle: Castle, enemyCastle: Castle) {
            val world = gameScreen.world

            val background = GameObject(AssetManager.instance.getDrawable(CastleGame.SKY).textureRegion, world)
            background.setSize(viewportWidth, viewportHeight - 300f)
            background.setPosition(0f, 300f)
            world.addGameObject(background)
            gameScreen.background = background

            val clouds = Clouds(gameScreen)
            world.addGameObject(clouds)

            playerCastle.setPosition(300f, 300f)
            playerCastle.color = CastleGame.GREY
            world.addGameObject(playerCastle)

            enemyCastle.setPosition(viewportWidth - 300f - enemyCastle.width, 300f)
            enemyCastle.color = CastleGame.RED
            enemyCastle.flipped = true
            world.addGameObject(enemyCastle)

            val ground = GameObject(AssetManager.instance.getDrawable(CastleGame.GROUND).textureRegion, world)
            ground.setSize(viewportWidth, 280f)
            world.addGameObject(ground)
            gameScreen.ground = ground

            val grass = GameObject(AssetManager.instance.getDrawable(CastleGame.GRASS).textureRegion, world)
            grass.setSize(viewportWidth, 30f)
            grass.setPosition(0f, 275f)
            world.addGameObject(grass)
            gameScreen.grass = grass
        }

        fun setupCards(gameScreen: GameScreen, world: World, garbageCan: GarbageCan) {
            garbageCan.visibility = Visibility.GONE
            world.addGameObject(garbageCan)
        }

        fun setupForeground(gameScreen: GameScreen, foregroundViewportWidth: Float, foregroundViewportHeight: Float) {
            val world = gameScreen.world
            val inventoryBox: InventoryBox = InventoryBox(world, null, CardType.WEAPON)
            inventoryBox.x = 25f
            inventoryBox.y = getInventoryBoxPosition(1, 3, foregroundViewportHeight)
            inventoryBox.visibility = Visibility.GONE
            inventoryBox.id = R.id.p_1_army_inventory
            world.addView(inventoryBox)

            val inventoryBox2: InventoryBox = InventoryBox(world, null, CardType.MAGIC)
            inventoryBox2.x = 25f
            inventoryBox2.y = getInventoryBoxPosition(2, 3, foregroundViewportHeight)
            inventoryBox2.visibility = Visibility.GONE
            inventoryBox2.id = R.id.p_1_gem_inventory
            world.addView(inventoryBox2)

            val inventoryBox3: InventoryBox = InventoryBox(world, null, CardType.CONSTRUCTION)
            inventoryBox3.x = 25f
            inventoryBox3.y = getInventoryBoxPosition(3, 3, foregroundViewportHeight)
            inventoryBox3.visibility = Visibility.GONE
            inventoryBox3.id = R.id.p_1_builder_inventory
            world.addView(inventoryBox3)

            val inventoryBox4: InventoryBox = InventoryBox(world, null, CardType.WEAPON)
            inventoryBox4.x = foregroundViewportWidth - 25f - inventoryBox4.layoutConstraints.width
            inventoryBox4.y = getInventoryBoxPosition(1, 3, foregroundViewportHeight)
            inventoryBox4.flipped = true
            inventoryBox4.visibility = Visibility.GONE
            inventoryBox4.id = R.id.p_2_army_inventory
            world.addView(inventoryBox4)

            val inventoryBox5: InventoryBox = InventoryBox(world, null, CardType.MAGIC)
            inventoryBox5.x = foregroundViewportWidth - 25f - inventoryBox5.layoutConstraints.width
            inventoryBox5.y = getInventoryBoxPosition(2, 3, foregroundViewportHeight)
            inventoryBox5.flipped = true
            inventoryBox5.visibility = Visibility.GONE
            inventoryBox5.id = R.id.p_2_gem_inventory
            world.addView(inventoryBox5)

            val inventoryBox6: InventoryBox = InventoryBox(world, null, CardType.CONSTRUCTION)
            inventoryBox6.x = foregroundViewportWidth - 25f - inventoryBox6.layoutConstraints.width
            inventoryBox6.y = getInventoryBoxPosition(3, 3, foregroundViewportHeight)
            inventoryBox6.flipped = true
            inventoryBox6.visibility = Visibility.GONE
            inventoryBox6.id = R.id.p_2_builder_inventory
            world.addView(inventoryBox6)

            val castleGameSignDrawable = AssetManager.instance.getDrawable(CastleGame.CASTLE_WARS_SIGN)
            val castleGameSign = View(world, GameScreenUI.getSignLayoutConstraints(foregroundViewportWidth, foregroundViewportHeight, castleGameSignDrawable))
            castleGameSign.backgroundDrawable = castleGameSignDrawable
            castleGameSign.id = R.id.castle_wars_sign
            world.addView(castleGameSign)

            val playButton = ButtonMenuCombo(
                    world,
                    GameScreenUI.getPlayCardButtonLayoutConstraints(foregroundViewportWidth),
                    GameScreenUI.getPlayMenuLayoutConstraints(foregroundViewportWidth, foregroundViewportHeight),
                    AssetManager.instance.getDrawable(CastleGame.IC_PLAY),
                    gameScreen
            )
            GameScreenUI.getPlayCardMenuItems(gameScreen, playButton).forEach {
                it.visibility = Visibility.GONE
                playButton.addView(it)
            }
            playButton.backgroundDrawable = AssetManager.instance.getDrawable(CastleGame.MENU_CARD)
            playButton.id = R.id.play_menu
            world.addView(playButton)

            val shoppingButton = ButtonMenuCombo(
                    world,
                    GameScreenUI.getShoppingCardButtonLayoutConstraints(foregroundViewportWidth),
                    GameScreenUI.getShoppingMenuLayoutConstraints(foregroundViewportWidth, foregroundViewportHeight),
                    AssetManager.instance.getDrawable(CastleGame.IC_SHOPPING_CART),
                    gameScreen
            )
            GameScreenUI.getShoppingCardMenuItems(gameScreen, shoppingButton).forEach {
                it.visibility = Visibility.GONE
                shoppingButton.addView(it)
            }
            shoppingButton.backgroundDrawable = AssetManager.instance.getDrawable(CastleGame.MENU_CARD)
            shoppingButton.id = R.id.shop_menu
            world.addView(shoppingButton)

            val settingsButton = ButtonMenuCombo(
                    world,
                    GameScreenUI.getSettingsCardButtonLayoutConstraints(foregroundViewportWidth),
                    GameScreenUI.getSettingsMenuLayoutConstraints(foregroundViewportWidth, foregroundViewportHeight),
                    AssetManager.instance.getDrawable(CastleGame.IC_SETTINGS),
                    gameScreen
            )
            GameScreenUI.getSettingsCardMenuItems(gameScreen, settingsButton).forEach {
                it.visibility = Visibility.GONE
                settingsButton.addView(it)
            }
            settingsButton.backgroundDrawable = AssetManager.instance.getDrawable(CastleGame.MENU_CARD)
            settingsButton.id = R.id.settings_menu
            world.addView(settingsButton)

            val inGameSettingsButton = SettingsMenu(
                    world,
                    GameScreenUI.getSettingsMenuCardButtonLayoutConstraint(foregroundViewportWidth),
                    GameScreenUI.getSettingsMenuCardLayoutConstraints(foregroundViewportWidth),
                    AssetManager.instance.getDrawable(CastleGame.IC_SETTINGS)
            )
            inGameSettingsButton.visibility = Visibility.GONE
            GameScreenUI.getInGameSettingsButtonItems(gameScreen, inGameSettingsButton).forEach {
                it.visibility = Visibility.GONE
                inGameSettingsButton.addView(it)
            }
            inGameSettingsButton.backgroundDrawable = AssetManager.instance.getDrawable(CastleGame.MENU_CARD)
            inGameSettingsButton.id = R.id.settings_menu_in_game
            world.addView(inGameSettingsButton)

            world.addView(SplashView(gameScreen, AssetManager.instance.getDrawable(CastleGame.FLAME_GROUND),
                    AssetManager.instance.getDrawable(CastleGame.DESITUM_1024)))

        }

        fun setupMenus(gameScreen: GameScreen) {

        }


        val PLAY_MENU_WIDTH = GameScreen.VIEWPORT_WIDTH / (3f / 2f)
        val PLAY_MENU_HEIGHT = GameScreen.VIEWPORT_HEIGHT / (3f / 2f)
        @JvmStatic fun getPlayMenuLayoutConstraints(foregroundViewportWidth: Float, foregroundViewportHeight: Float): LayoutConstraints {
            return LayoutConstraints(
                    foregroundViewportWidth / 2 - PLAY_MENU_WIDTH / 2,
                    foregroundViewportHeight / 2 - PLAY_MENU_HEIGHT / 2,
                    PLAY_MENU_WIDTH, PLAY_MENU_HEIGHT
            )
        }

        @JvmStatic fun getPlayCardButtonLayoutConstraints(foregroundViewportWidth: Float): LayoutConstraints {
            return LayoutConstraints(
                    foregroundViewportWidth / 2 - Card.WIDTH * 1.7f,
                    Card.CARD_Y,
                    Card.WIDTH,
                    Card.HEIGHT
            )
        }

        @JvmStatic fun getSettingsMenuCardButtonLayoutConstraint(foregroundViewportWidth: Float): LayoutConstraints {
            return LayoutConstraints(
                    foregroundViewportWidth - 55 - Card.WIDTH,
                    Card.CARD_Y,
                    Card.WIDTH, Card.HEIGHT
            )
        }

        @JvmStatic fun getSettingsMenuCardLayoutConstraints(foregroundViewportWidth: Float): LayoutConstraints {
            return LayoutConstraints(
                    foregroundViewportWidth / 2 - Card.WIDTH * 1.7f,
                    Card.CARD_Y,
                    Card.WIDTH,
                    Card.HEIGHT * 2
            )
        }

        @JvmStatic fun getSettingsMenuLayoutConstraints(foregroundViewportWidth: Float, foregroundViewportHeight: Float): LayoutConstraints {
            return LayoutConstraints(
                    foregroundViewportWidth / 2 - PLAY_MENU_WIDTH / 2,
                    foregroundViewportHeight / 2 - PLAY_MENU_HEIGHT / 1.8f,
                    PLAY_MENU_WIDTH, PLAY_MENU_HEIGHT / .8f
            )
        }

        @JvmStatic fun getSettingsCardButtonLayoutConstraints(foregroundViewportWidth: Float): LayoutConstraints {
            return LayoutConstraints(
                    foregroundViewportWidth / 2 + Card.WIDTH * .7f,
                    Card.CARD_Y,
                    Card.WIDTH,
                    Card.HEIGHT
            )
        }

        @JvmStatic fun getShoppingMenuLayoutConstraints(foregroundViewportWidth: Float, foregroundViewportHeight: Float): LayoutConstraints {
            return getSettingsMenuLayoutConstraints(foregroundViewportWidth, foregroundViewportHeight)
        }

        @JvmStatic fun getShoppingCardButtonLayoutConstraints(foregroundViewportWidth: Float): LayoutConstraints {
            return LayoutConstraints(
                    foregroundViewportWidth / 2 - Card.WIDTH * .500001f,
                    Card.CARD_Y,
                    Card.WIDTH,
                    Card.HEIGHT
            )
        }

        @JvmStatic fun getPlayCardMenuItems(gameScreen: GameScreen, playButton: ButtonMenuCombo): Array<View> {
            val world = gameScreen.world

            val easyImage = AssetManager.instance.getDrawable(CastleGame.EASY)
            val hardImage = AssetManager.instance.getDrawable(CastleGame.HARD)
            val passAndPlayImage = AssetManager.instance.getDrawable(CastleGame.P_AND_P)
            val backgroundImage = AssetManager.instance.getDrawable(CastleGame.THEME_SELECTION)

            val themeTextViewLC = LayoutConstraints(playButton.width / 4, 0f, playButton.width / 2, 50f, alignParentTop = true, marginTop = 50f, centerHorizontal = true)
            val themeTextView = TextView(world, themeTextViewLC, AssetManager.instance.getFont(CastleGame.MAIN_FONT))
            themeTextView.text = "THEME"

            val easyButtonLayoutConstraints = LayoutConstraints(
                    PLAY_MENU_WIDTH / 9,
                    (PLAY_MENU_HEIGHT / 9) * 5,
                    (PLAY_MENU_WIDTH / 9) * 3,
                    (PLAY_MENU_HEIGHT / 9) * 3
            )

            val easyButton = MenuItemView(world, easyButtonLayoutConstraints, easyImage)
            easyButton.backgroundDrawable = backgroundImage.clone()
            easyButton.color = CastleGame.GREY
            easyButton.id = R.id.button_easy
            easyButton.highlighted = true

            val hardButtonLayoutConstraints = LayoutConstraints(
                    PLAY_MENU_WIDTH / 9 * 5,
                    (PLAY_MENU_HEIGHT / 9) * 5,
                    (PLAY_MENU_WIDTH / 9) * 3,
                    (PLAY_MENU_HEIGHT / 9) * 3
            )
            val hardButton = MenuItemView(world, hardButtonLayoutConstraints, hardImage)
            hardButton.backgroundDrawable = backgroundImage.clone()
            hardButton.color = CastleGame.RED
            hardButton.id = R.id.button_hard
            hardButton.highlighted = true

            val passAndPlayLayoutConstraints = LayoutConstraints(
                    PLAY_MENU_WIDTH / 9 * 3,
                    (PLAY_MENU_HEIGHT / 9) * 1,
                    (PLAY_MENU_WIDTH / 9) * 3,
                    (PLAY_MENU_HEIGHT / 9) * 3
            )
            val passAndPlayButton = MenuItemView(world, passAndPlayLayoutConstraints, passAndPlayImage)
            passAndPlayButton.backgroundDrawable = backgroundImage.clone()
            passAndPlayButton.color = CastleGame.PURPLE
            passAndPlayButton.id = R.id.button_pass_and_play
            passAndPlayButton.highlighted = true

            return arrayOf(easyButton, hardButton, passAndPlayButton)
        }

        @JvmStatic fun getShoppingCardMenuItems(gameScreen: GameScreen, playButton: ButtonMenuCombo): Array<View> {
            val world = gameScreen.world

            val cardSlot = AssetManager.instance.getDrawable(CastleGame.CARD_PLUS_1)
            val samurai = AssetManager.instance.getDrawable(CastleGame.IC_SAMURAI)
            val flame = AssetManager.instance.getDrawable(CastleGame.IC_FIRE)
            val backgroundImage = AssetManager.instance.getDrawable(CastleGame.THEME_SELECTION)

            val shoppingTextViewLC = LayoutConstraints(playButton.width / 4, 0f, playButton.width / 2, 50f, alignParentTop = true, marginTop = 50f, centerHorizontal = true)
            val shoppingTextView = TextView(world, shoppingTextViewLC, AssetManager.instance.getFont(CastleGame.MAIN_FONT))
            shoppingTextView.text = "SHOP"

            val flameShoppingViewLayoutConstraints = LayoutConstraints(
                    PLAY_MENU_WIDTH / 9,
                    (PLAY_MENU_HEIGHT / 9) * 5,
                    (PLAY_MENU_WIDTH / 9) * 3,
                    (PLAY_MENU_HEIGHT / 9) * 3
            )
            val flameShoppingView = ShoppingView(world, flameShoppingViewLayoutConstraints, flame)
            flameShoppingView.backgroundDrawable = backgroundImage.clone()
            flameShoppingView.color = CastleGame.RED
            flameShoppingView.id = R.id.shopping_flame

            val japaneseShoppingViewLayoutConstraints = LayoutConstraints(
                    PLAY_MENU_WIDTH / 9 * 5,
                    (PLAY_MENU_HEIGHT / 9) * 5,
                    (PLAY_MENU_WIDTH / 9) * 3,
                    (PLAY_MENU_HEIGHT / 9) * 3
            )
            val japaneseShoppingView = ShoppingView(world, japaneseShoppingViewLayoutConstraints, samurai)
            japaneseShoppingView.backgroundDrawable = backgroundImage.clone()
            japaneseShoppingView.color = CastleGame.PURPLE
            japaneseShoppingView.id = R.id.shopping_japanese

            val extraCard1LayoutConstraints = LayoutConstraints(
                    PLAY_MENU_WIDTH / 9 * 1,
                    (PLAY_MENU_HEIGHT / 9) * 1,
                    (PLAY_MENU_WIDTH / 9) * 3,
                    (PLAY_MENU_HEIGHT / 9) * 3
            )

            val extraCard1ShoppingView = ShoppingView(world, extraCard1LayoutConstraints, cardSlot)
            extraCard1ShoppingView.backgroundDrawable = backgroundImage.clone()
            extraCard1ShoppingView.color = CastleGame.GREEN
            extraCard1ShoppingView.id = R.id.shopping_card_1

            val extraCard2LayoutConstraints = LayoutConstraints(
                    PLAY_MENU_WIDTH / 9 * 5,
                    (PLAY_MENU_HEIGHT / 9) * 1,
                    (PLAY_MENU_WIDTH / 9) * 3,
                    (PLAY_MENU_HEIGHT / 9) * 3
            )
            val extraCard2ShoppingView = ShoppingView(world, extraCard2LayoutConstraints, cardSlot)
            extraCard2ShoppingView.backgroundDrawable = backgroundImage.clone()
            extraCard2ShoppingView.color = CastleGame.BLUE
            extraCard2ShoppingView.id = R.id.shopping_card_2


            return arrayOf(shoppingTextView, flameShoppingView, japaneseShoppingView, extraCard1ShoppingView, extraCard2ShoppingView)
        }

        @JvmStatic fun getSettingsCardMenuItems(gameScreen: GameScreen, playButton: ButtonMenuCombo): Array<View> {
            val world = gameScreen.world

            val legion = AssetManager.instance.getDrawable(CastleGame.IC_LEGION)
            val samurai = AssetManager.instance.getDrawable(CastleGame.IC_SAMURAI)
            val flame = AssetManager.instance.getDrawable(CastleGame.IC_FIRE)
            val backgroundImage = AssetManager.instance.getDrawable(CastleGame.THEME_SELECTION)

            val themeTextViewLC = LayoutConstraints(playButton.width / 4, 0f, playButton.width / 2, 50f, alignParentTop = true, marginTop = 50f, centerHorizontal = true)
            val themeTextView = TextView(world, themeTextViewLC, AssetManager.instance.getFont(CastleGame.MAIN_FONT))
            themeTextView.text = "THEME"

            val defaultThemeViewLayoutConstraints = LayoutConstraints(
                    PLAY_MENU_WIDTH / 9,
                    (PLAY_MENU_HEIGHT / 9) * 5,
                    (PLAY_MENU_WIDTH / 9) * 3,
                    (PLAY_MENU_HEIGHT / 9) * 3
            )
            val defaultThemeView = MenuItemView(world, defaultThemeViewLayoutConstraints, legion)
            defaultThemeView.backgroundDrawable = backgroundImage.clone()
            defaultThemeView.color = CastleGame.GREY
            defaultThemeView.id = R.id.theme_default

            val flameThemeViewLayoutConstraints = LayoutConstraints(
                    PLAY_MENU_WIDTH / 9 * 5,
                    (PLAY_MENU_HEIGHT / 9) * 5,
                    (PLAY_MENU_WIDTH / 9) * 3,
                    (PLAY_MENU_HEIGHT / 9) * 3
            )
            val flameThemeView = MenuItemView(world, flameThemeViewLayoutConstraints, flame)
            flameThemeView.backgroundDrawable = backgroundImage.clone()
            flameThemeView.color = CastleGame.RED
            flameThemeView.id = R.id.theme_flame

            val japaneseThemeViewLayoutConstraints = LayoutConstraints(
                    PLAY_MENU_WIDTH / 9 * 3,
                    (PLAY_MENU_HEIGHT / 9) * 1,
                    (PLAY_MENU_WIDTH / 9) * 3,
                    (PLAY_MENU_HEIGHT / 9) * 3
            )
            val japaneseThemeView = MenuItemView(world, japaneseThemeViewLayoutConstraints, samurai)
            japaneseThemeView.backgroundDrawable = backgroundImage.clone()
            japaneseThemeView.color = CastleGame.PURPLE
            japaneseThemeView.id = R.id.theme_japanese

            defaultThemeView.highlighted = true
            return arrayOf(themeTextView, defaultThemeView, flameThemeView, japaneseThemeView)
        }

        @JvmStatic fun getInGameSettingsButtonItems(gameScreen: GameScreen, playButton: SettingsMenu): Array<View> {

            val ICON_WIDTH = playButton.layoutConstraints.width / 2f
            val ICON_HEIGHT = playButton.layoutConstraints.width / 2f
            val world = gameScreen.world

            val volumeButtonImage = AssetManager.instance.getDrawable(CastleGame.IC_VOLUME_ON)
            val volumeMuteButtonImage = AssetManager.instance.getDrawable(CastleGame.IC_VOLUME_OFF)
            volumeButtonImage.color = CastleGame.GREY
            volumeMuteButtonImage.color = CastleGame.GREY
            val homeButtonImage = AssetManager.instance.getDrawable(CastleGame.IC_HOME)
            homeButtonImage.color = CastleGame.GREY

            val volumeButton = ToggleButton(world, LayoutConstraints(
                    0f, playButton.layoutConstraints.height - playButton.ICON_HEIGHT / 2,
                    ICON_WIDTH, ICON_HEIGHT, centerHorizontal = true
            ), volumeButtonImage, volumeMuteButtonImage)
            volumeButton.color = Color(CastleGame.GREY.r, CastleGame.GREY.g, CastleGame.GREY.b, 1f)
            volumeButton.id = R.id.button_volume

            val homeButton = Button(world, LayoutConstraints(
                    0f, playButton.layoutConstraints.height * 1.5f - playButton.ICON_HEIGHT / 2,
                    ICON_WIDTH, ICON_HEIGHT, centerHorizontal = true
            ))
            homeButton.restDrawable = homeButtonImage
            homeButton.color = CastleGame.GREY
            homeButton.id = R.id.button_home

            return arrayOf(volumeButton, homeButton)
        }

        @JvmStatic fun getSignLayoutConstraints(foregroundViewportWidth: Float, foregroundViewportHeight: Float,
                                                signDrawable: Drawable): LayoutConstraints {
            return LayoutConstraints(
                    foregroundViewportWidth / 2 - signDrawable.width / 2,
                    foregroundViewportHeight - (signDrawable.height + foregroundViewportHeight / 20f),
                    signDrawable.width, signDrawable.height
            )
        }

        @JvmStatic fun getInventoryBoxPosition(position: Int, total: Int, viewportHeight: Float): Float {
            return (((viewportHeight - GROUND_HEIGHT) / (total * 2)) * (position * 2 - 1)) - InventoryBox.HEIGHT / 2 + GROUND_HEIGHT
        }

        val GROUND_HEIGHT = 305f
    }

}
