package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.CastleGame
import com.desitum.library.drawing.Drawable
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.view.LayoutConstraints
import com.desitum.library.view.View

/**
 * Created by kodyvanry on 8/15/17.
 */
class MenuItemView(world: World, layoutConstraints: LayoutConstraints, val icon: Drawable) : View(world, layoutConstraints) {

    var highlighted: Boolean = false
    val overlay = AssetManager.instance.getDrawable(CastleGame.THEME_SELECT_OVERLAY)

    init {
        clickable = true
    }

    override fun onDraw(batch: Batch, viewport: Viewport) {
        super.onDraw(batch, viewport)
        icon.draw(batch,
                x + width / 2f - icon.width / 2,
                y + height / 2f - icon.height / 2,
                icon.width,
                icon.height)
        if (!highlighted) {
            overlay.draw(batch, x, y, width, height)
        }
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        icon?.color = Color(icon.color.r, icon.color.g, icon.color.b, a)
        overlay?.color = Color(overlay.color.r, overlay.color.g, overlay.color.b, a)
    }

    override fun setColor(color: Color) {
        super.setColor(color)
        icon?.color = Color(icon.color.r, icon.color.g, icon.color.b, color.a)
        overlay?.color = Color(overlay.color.r, overlay.color.g, overlay.color.b, color.a)
    }

}