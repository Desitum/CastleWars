package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.CastleGame
import com.desitum.library.animation.RotateAnimator
import com.desitum.library.drawing.Drawable
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.game_objects.GameObject
import com.desitum.library.interpolation.Interpolation

/**
 * Created by kodyvanry on 6/8/17.
 */
class GarbageCan(world: World?) : GameObject(null, world) {

    private var lid: GameObject = GameObject(AssetManager.instance.getDrawable(CastleGame.GARBAGE_LID).textureRegion, null)
    private var bottom: Drawable = AssetManager.instance.getDrawable(CastleGame.GARBAGE_BODY)
    private var opening = false
    private var closing = false

    private val LID_HEIGHT = 25f
    private val LID_GAP = 10f
    private val LID_Y: Float
        get() {
            return y + height - LID_HEIGHT
        }

    init {
        setSize(100f, 165f)
        setPosition(55f, 55f)
    }

    override fun onDraw(spriteBatch: Batch, viewport: Viewport) {
        super.onDraw(spriteBatch, viewport)
        lid.draw(spriteBatch, viewport)
        bottom.draw(spriteBatch, x, y, width, height - LID_HEIGHT - LID_GAP)
        lid.draw(spriteBatch, viewport)
    }

    override fun update(delta: Float) {
        super.update(delta)
        lid.update(delta)
    }

    override fun draw(batch: Batch?) {
        super.draw(batch)
    }

    override fun setX(x: Float) {
        super.setX(x)
        lid.x = x
    }

    override fun setY(y: Float) {
        super.setY(y)
        lid.y = LID_Y
    }

    override fun setPosition(x: Float, y: Float) {
        super.setPosition(x, y)
        lid.x = x
        lid.y = LID_Y
    }

    override fun setSize(width: Float, height: Float) {
        super.setSize(width, height)
        lid.y = LID_Y
        lid.setSize(width, LID_HEIGHT)
    }

    fun open() {
        if (opening)
            return
        closing = false
        lid.animators.clear();
        lid.addAndStartAnimator(
                RotateAnimator(
                        lid,
                        0.25f, 0f,
                        lid.rotation, 30f,
                        Interpolation.DECELERATE_INTERPOLATOR
                )
        )
    }

    fun close() {
        if (closing)
            return
        opening = false
        lid.animators.clear();
        lid.addAndStartAnimator(
                RotateAnimator(
                        lid,
                        0.25f, 0f,
                        lid.rotation, 0f,
                        Interpolation.DECELERATE_INTERPOLATOR
                )
        )
    }

    override fun setColor(tint: Color?) {
        super.setColor(tint)
        tint?.let {
            lid.color = it
            bottom.color = it
        }
    }
}