package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.CastleGame
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.view.*

/**
 * Created by kodyvanry on 7/31/17
 */
class CardDescriptionView constructor(world: World, layoutConstraints: LayoutConstraints, var font: BitmapFont, var text: String) : View(world, layoutConstraints) {

    var alignment: Int = 0
    private val glyphLayout: GlyphLayout = GlyphLayout(font, text)
    var textColor: Color
    val arrow = AssetManager.instance.getDrawable(CastleGame.CARD_DESCRIPTION_ARROW)

    init {
        val cX = layoutConstraints.x + layoutConstraints.width / 2
        layoutConstraints.width = textWidth * 1.2f
        layoutConstraints.x = cX - layoutConstraints.width / 2
        layoutConstraints.height = textHeight * 1.6f
//        text = text.replace("\n", " ")
        glyphLayout.setText(font, text.replace("\n", " "), Color.BLACK, textWidth, Align.center, true)
        textColor = Color.BLACK
        this.alignment = LinearLayout.ALIGNMENT_CENTER
        backgroundDrawable = AssetManager.instance.getDrawable(CastleGame.CARD_DESCRIPTION_BOX)

    }

    override fun draw(batch: Batch, viewport: Viewport) {
        arrow.draw(batch, x + width / 2 - ARROW_SIZE / 2, y - ARROW_SIZE / 2, ARROW_SIZE, ARROW_SIZE)
        super.draw(batch, viewport)
    }

    override fun update(delta: Float) {
        super.update(delta)
    }

    override fun onTouchEvent(touchEvent: TouchEvent): Boolean {
        return super.onTouchEvent(touchEvent)
    }

    override fun onDraw(batch: Batch, viewport: Viewport) {
        super.onDraw(batch, viewport)

        if (alignment == LinearLayout.ALIGNMENT_LEFT)
            font.draw(batch, glyphLayout, x + height * 0.2f, y + height * 0.8f)
        if (alignment == LinearLayout.ALIGNMENT_CENTER)
            font.draw(batch, glyphLayout, x + width / 2 - textWidth / 2, y + height * 0.8f)
        if (alignment == LinearLayout.ALIGNMENT_RIGHT)
            font.draw(batch, glyphLayout, x + width - textWidth - height * 0.2f, y + height * 0.8f)
    }

    var _textWidth: Float? = null
    val textWidth: Float
        get() {
            _textWidth?.let { return it }
            glyphLayout.setText(font, text)
            _textWidth = glyphLayout.width
            return _textWidth!!
        }

    var _textHeight: Float? = null
    val textHeight: Float
        get() {
            _textHeight?.let { return it }
            glyphLayout.setText(font, text)
            _textHeight = glyphLayout.height
            return _textHeight!!
        }

    companion object {
        val ARROW_SIZE = 50f
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        glyphLayout?.setText(font, text, Color(0f, 0f, 0f, a), textWidth, Align.center, true)
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
        glyphLayout?.setText(font, text, Color(textColor.r, textColor.g, textColor.b, tint.a), textWidth, Align.center, true)
    }
}