package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.GameScreen
import com.desitum.castlewars.fadeOut
import com.desitum.library.drawing.Drawable
import com.desitum.library.view.LayoutConstraints
import com.desitum.library.view.View

/**
 * Created by kodyvanry on 8/23/17
 */
class SplashView(gameScreen: GameScreen, background: Drawable, val logo: Drawable) : View(gameScreen.world, LayoutConstraints(0f, 0f, gameScreen.foregroundViewportWidth, gameScreen.viewportHeight)) {

    init {
        backgroundDrawable = background
        z = 1000
        clickable = true
        focusable = true
        fadeOut(1f, 0.5f)
    }

    override fun onDraw(batch: Batch, viewport: Viewport) {
        super.onDraw(batch, viewport)
        logo.draw(batch,
                width / 2 - logo.width / 2,
                height / 2 - logo.height / 2,
                logo.width, logo.height)
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        logo?.color = Color(r, g, b, a)
    }

    override fun setColor(color: Color) {
        super.setColor(color)
        logo?.color = color
    }

}