package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.CastleGame
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.view.*

/**
 * Created by kodyvanry on 7/31/17
 */
class TutorialAlertView constructor(world: World, layoutConstraints: LayoutConstraints, var font: BitmapFont, var text: String) : View(world, layoutConstraints) {

    val arrow = AssetManager.instance.getDrawable(CastleGame.CARD_DESCRIPTION_ARROW)

    init {
        backgroundDrawable = AssetManager.instance.getDrawable(CastleGame.CARD_DESCRIPTION_BOX)

    }

    override fun draw(batch: Batch, viewport: Viewport) {
        arrow.draw(batch, x + width / 2 - ARROW_SIZE / 2, y - ARROW_SIZE / 2, ARROW_SIZE, ARROW_SIZE)
        super.draw(batch, viewport)
    }

    override fun update(delta: Float) {
        super.update(delta)
    }

    override fun onTouchEvent(touchEvent: TouchEvent): Boolean {
        return super.onTouchEvent(touchEvent)
    }

    override fun onDraw(batch: Batch, viewport: Viewport) {
        super.onDraw(batch, viewport)
    }

    companion object {
        val ARROW_SIZE = 50f
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
    }
}