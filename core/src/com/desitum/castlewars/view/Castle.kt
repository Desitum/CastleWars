package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.NumberUtils
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.CastleGame
import com.desitum.castlewars.clone
import com.desitum.castlewars.fadeIn
import com.desitum.castlewars.fadeOut
import com.desitum.library.animation.ValueAnimator
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.game_objects.GameObject
import com.desitum.library.game_objects.Visibility
import com.desitum.library.interpolation.Interpolation
import com.desitum.library.particles.ParticleEmitter
import com.desitum.library.particles.ParticleSettings
import com.desitum.library.view.LayoutConstraints
import com.desitum.library.view.TextView

/**
 * Created by kodyvanry on 6/8/17.
 */
class Castle(val _world: World) : GameObject(null, _world) {

    var wallAnimator: ValueAnimator? = null
    var castleAnimator: ValueAnimator? = null
    var castleLifeText: TextView
    var wallLifeText: TextView
    val castleParticles: ParticleEmitter
    val wallParticles: ParticleEmitter


    var castleLife: Int = 60
        set(value) {
            castleAnimator = ValueAnimator(0.5f, field.toFloat(), Math.max(Math.min(value.toFloat(), 100f), 0f), 0f, Interpolation.LINEAR_INTERPOLATOR)
            castleAnimator!!.addListener { onCastleAnimatorUpdate(it as ValueAnimator) }
            addAndStartAnimator(castleAnimator!!)
            if (field != value) {
                castleParticles.x = castleX
                castleParticles.width = CASTLE_WIDTH
                castleParticles.height = 0.1f
                castleParticles.y = y
                castleAnimator!!.onAnimationFinished = { castleParticles.turnOff() }
                castleParticles.turnOn()
            }
            field = Math.max(0, Math.min(100, value))
        }

    var wallLife: Int = 20
        set(value) {
            wallAnimator = ValueAnimator(0.5f, field.toFloat(), Math.max(Math.min(value.toFloat(), 100f), 0f), 0f, Interpolation.LINEAR_INTERPOLATOR)
            wallAnimator!!.addListener { onWallAnimatorUpdate(it as ValueAnimator) }
            addAndStartAnimator(wallAnimator!!)
            if (field != value) {
                wallParticles.x = wallX
                wallParticles.width = WALL_WIDTH
                wallParticles.height = 0.1f
                wallParticles.y = y
                wallAnimator!!.onAnimationFinished = { wallParticles.turnOff() }
                wallParticles.turnOn()
            }
            field = Math.max(0, Math.min(100, value))
        }

    var castleDisplayLife: Float = castleLife.toFloat()
    var wallDisplayLife: Float = wallLife.toFloat()

    var flipped: Boolean = false
        @JvmName("flip") set(value) {
            field = value
            wallLifeText.layoutConstraints.x = (wallX + WALL_WIDTH / 2 - wallLifeText.layoutConstraints.width / 2)
            wallLifeText.invalidate()
            castleLifeText.layoutConstraints.x = (castleX + CASTLE_WIDTH / 2 - castleLifeText.layoutConstraints.width / 2)
            castleLifeText.invalidate()
            castleParticles.x = castleX
            wallParticles.x = wallX
        }
        @JvmName("isFlipped") get

    val castleX: Float
        get() = if (flipped) x + width - CASTLE_WIDTH else x

    val castleY: Float
        get() = y - CASTLE_HEIGHT + (castleDisplayLife / MAX_CASTLE_LIFE) * CASTLE_HEIGHT

    val wallX: Float
        get() = if (flipped) x else x + width - WALL_WIDTH

    val wallY: Float
        get() = y - WALL_HEIGHT + (wallDisplayLife / MAX_WALL_LIFE) * WALL_HEIGHT

    val doorX: Float
        get() = castleX + (CASTLE_WIDTH / 2) - DOOR_WIDTH / 2

    val doorY: Float
        get() = Math.min(y, castleY + CASTLE_HEIGHT - DOOR_HEIGHT * 2)

    var castle = AssetManager.instance.getDrawable(CastleGame.CASTLE)
        set(value) {
            value.color = field.color
            field = value
        }
    var wall = AssetManager.instance.getDrawable(CastleGame.WALL)
        set(value) {
            value.color = field.color
            field = value
        }
    val door = AssetManager.instance.getDrawable(CastleGame.WOOD_DOOR)
    var doorVisible = true

    init {
        setSize(CASTLE_WIDTH + WALL_CASTLE_GAP + WALL_WIDTH, CASTLE_HEIGHT)

        val cltLayoutConstraints = LayoutConstraints(100f, 100f, CASTLE_WIDTH, 75f)
        val castleLifeFont = AssetManager.instance.getFont(CastleGame.MAIN_FONT).clone()
        castleLifeFont.data.setScale(0.8f)
        castleLifeText = TextView(_world, cltLayoutConstraints, castleLifeFont)
        castleLifeText.text = "$castleLife"
        castleLifeText.textColor = CastleGame.WHITE
        castleLifeText.alignment = Align.center
        _world.addView(castleLifeText)
        val wltLayoutConstraints = LayoutConstraints(100f, 100f, CASTLE_WIDTH, 75f)
        val wallLifeFont = AssetManager.instance.getFont(CastleGame.MAIN_FONT).clone()
        wallLifeFont.data.setScale(0.8f)
        wallLifeText = TextView(_world, wltLayoutConstraints, wallLifeFont)
        wallLifeText.text = "$wallLife"
        wallLifeText.textColor = CastleGame.WHITE
        wallLifeText.alignment = Align.center
        _world.addView(wallLifeText)

        wallLifeText.visibility = Visibility.GONE
        castleLifeText.visibility = Visibility.GONE

        castleParticles = ParticleEmitter(castleX, y, 50f)
        wallParticles = ParticleEmitter(castleX, y, 15f)
        setupParticleEffects()
    }

    override fun update(delta: Float) {
        super.update(delta)
    }

    fun animateIn() {
        castleLife = STARTING_CASTLE_LIFE
        wallLife = STARTING_WALL_LIFE
        wallLifeText.fadeIn()
        castleLifeText.fadeIn()
    }

    fun animateOut() {
//        castleLife = STARTING_CASTLE_LIFE
//        wallLife = STARTING_WALL_LIFE
        wallLifeText.fadeOut()
        castleLifeText.fadeOut()
    }

    override fun onDraw(spriteBatch: Batch, viewport: Viewport) {
        super.onDraw(spriteBatch, viewport)

        castle.draw(spriteBatch, castleX, castleY, CASTLE_WIDTH, CASTLE_HEIGHT)
        wall.draw(spriteBatch, wallX, wallY, WALL_WIDTH, WALL_HEIGHT)
        if (doorVisible)
            door.draw(spriteBatch, doorX, doorY, DOOR_WIDTH, DOOR_HEIGHT)
    }

    companion object {

        const val CASTLE_WIDTH = 400f
        const val CASTLE_HEIGHT = CASTLE_WIDTH * 2.16666666f

        const val WALL_WIDTH = CASTLE_WIDTH * .1666666666f
        const val WALL_HEIGHT = CASTLE_HEIGHT

        const val WALL_CASTLE_GAP = 25f

        const val DOOR_WIDTH = 66f
        const val DOOR_HEIGHT = 100f

        const val MAX_CASTLE_LIFE = 100f
        const val MAX_WALL_LIFE = 100f

        const val STARTING_CASTLE_LIFE = 60
        const val STARTING_WALL_LIFE = 20

    }

    override fun setColor(color: Float) {
        super.setColor(color)
        castle.color = Color(NumberUtils.floatToIntColor(color))
        wall.color = Color(NumberUtils.floatToIntColor(color))
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
        castle.color = tint
        wall.color = tint
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        castle?.color = Color(r, g, b, a)
        wall?.color = Color(r, g, b, a)
    }

    override fun setPosition(x: Float, y: Float) {
        super.setPosition(x, y)
        wallLifeText.layoutConstraints.x = (wallX + WALL_WIDTH / 2 - wallLifeText.layoutConstraints.width / 2)
        wallLifeText.invalidate()
        castleLifeText.layoutConstraints.x = (castleX + CASTLE_WIDTH / 2 - castleLifeText.layoutConstraints.width / 2)
        castleLifeText.invalidate()
    }

    fun onWallAnimatorUpdate(animator: ValueAnimator) {
        wallDisplayLife = animator.animatedValue
        wallLifeText.y = wallY + WALL_HEIGHT + 5f
        wallLifeText.text = "${wallDisplayLife.toInt()}"
//        wallLifeText.text = "$wallDisplayLife"
        wallLifeText.invalidate()
    }

    fun onCastleAnimatorUpdate(animator: ValueAnimator) {
        castleDisplayLife = animator.animatedValue
        castleLifeText.y = castleY + CASTLE_HEIGHT + 5f
        castleLifeText.invalidate()
        castleLifeText.text = "${castleDisplayLife.toInt()}"
    }

    fun setupParticleEffects() {
        val particleSettings = ParticleSettings(
                15f, 45f, 5f, 15f, 0f, 0f, 5f, 10f, -40f, 40f, 10f, 30f, -500f, 500f, 1f, 1.5f, true, true
        )
        castleParticles.addParticleSettings(particleSettings)
        wallParticles.addParticleSettings(particleSettings)
        castleParticles.width = CASTLE_WIDTH
        castleParticles.height = 0.1f
        castleParticles.particleTexture = AssetManager.instance.getDrawable(CastleGame.GROUND).textureRegion
        wallParticles.width = WALL_WIDTH
        wallParticles.height = 0.1f
        wallParticles.particleTexture = AssetManager.instance.getDrawable(CastleGame.GROUND).textureRegion

        _world.addParticleEmitter(castleParticles)
        _world.addParticleEmitter(wallParticles)
    }
}