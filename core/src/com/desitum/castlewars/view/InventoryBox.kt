package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.CastleGame
import com.desitum.castlewars.clone
import com.desitum.castlewars.model.CardType
import com.desitum.library.animation.ColorEffects
import com.desitum.library.drawing.Drawable
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.view.LayoutConstraints
import com.desitum.library.view.LinearLayout
import com.desitum.library.view.TextView
import com.desitum.library.view.ViewGroup

/**
 * Created by kodyvanry on 6/10/17.
 */
class InventoryBox(world: World, layoutConstraints: LayoutConstraints?, cardType: CardType) : ViewGroup(world, layoutConstraints) {

    val GENERATOR_X: Float
        get() = if (flipped) x + (width / 4) * 3 - generatorDrawable.width / 2 else x + width / 4 - generatorDrawable.width / 2

    val GENERATOR_Y: Float
        get() = y + (height / 4) * 3 - generatorDrawable.height / 2

    val GENERATOR_WIDTH: Float
        get() = Math.min(64f, generatorDrawable.width)

    val GENERATOR_HEIGHT: Float
        get() = Math.min(64f, generatorDrawable.height)

    val GENERATOR_TEXT_X: Float
        get() = if (flipped) (layoutConstraints.width / 4) - GENERATOR_TEXT_WIDTH / 2 else (layoutConstraints.width / 4) * 3 - GENERATOR_TEXT_WIDTH / 2

    val GENERATOR_TEXT_Y: Float
        get() = (layoutConstraints.height / 4) * 3 - GENERATOR_TEXT_HEIGHT / 2

    val GENERATOR_TEXT_WIDTH = 80f

    val GENERATOR_TEXT_HEIGHT = 64f

    val ASSET_TEXT_X: Float
        get() = if (flipped) (layoutConstraints.width / 4) - ASSET_TEXT_WIDTH / 2 else (layoutConstraints.width / 4) * 3 - ASSET_TEXT_WIDTH / 2

    val ASSET_TEXT_Y: Float
        get() = (layoutConstraints.height / 4) - ASSET_TEXT_HEIGHT / 2

    val ASSET_TEXT_WIDTH = 100f

    val ASSET_TEXT_HEIGHT = 64f

    val ASSET_X: Float
        get() = if (flipped) x + (width / 4) * 3 - assetDrawable.width / 2 else x + width / 4 - assetDrawable.width / 2

    val ASSET_Y: Float
        get() = y + (height / 4) - assetDrawable.height / 2

    val ASSET_WIDTH: Float
        get() = Math.min(64f, assetDrawable.width)

    val ASSET_HEIGHT: Float
        get() = Math.min(64f, assetDrawable.height)

    var generatorDrawable: Drawable
    var assetDrawable: Drawable
    var generatorTextView: TextView
    var assetTextView: TextView
    var generatorCount: Int = 200
        set(value) {
            if (field != value) {
                generatorTextView.text = "$value"
                generatorColorAnimator = ColorEffects(null, 0.2f, 0f, generatorTextView.textColor, redOrGreen(field, value))
                generatorColorAnimator!!.onAnimationFinished = {
                    generatorColorAnimator = ColorEffects(null, 0.2f, 0f, generatorTextView.textColor, CastleGame.WHITE)
                    generatorColorAnimator!!.start()
                }
                field = value
            }
        }

    var assetCount: Int = 10
        set(value) {
            if (field != value) {
                assetTextView.text = "$value"
                assetColorAnimator = ColorEffects(null, 0.2f, 0f, assetTextView.textColor, redOrGreen(field, value))
                assetColorAnimator!!.start()
                assetColorAnimator!!.onAnimationFinished = {
                    assetColorAnimator = ColorEffects(null, 0.2f, 0f, assetTextView.textColor, CastleGame.WHITE)
                    assetColorAnimator!!.start()
                }
                field = value
            }
        }

    var generatorColorAnimator: ColorEffects? = null
    var assetColorAnimator: ColorEffects? = null

    var flipped: Boolean = false
        set(value) {
            field = value
            generatorTextView.x = GENERATOR_TEXT_X
            assetTextView.x = ASSET_TEXT_X
        }

    init {
        setSize(WIDTH, HEIGHT)
        backgroundDrawable = AssetManager.instance.getDrawable(CastleGame.ASSET_BOX)
        when(cardType) {
            CardType.CONSTRUCTION -> {
                color = CardType.CONSTRUCTION.color
                generatorDrawable = AssetManager.instance.getDrawable(CastleGame.PICK_AXE)
                assetDrawable = AssetManager.instance.getDrawable(CastleGame.STONE)
            }
            CardType.MAGIC -> {
                color = CardType.MAGIC.color
                generatorDrawable = AssetManager.instance.getDrawable(CastleGame.WIZARD)
                assetDrawable = AssetManager.instance.getDrawable(CastleGame.WHITE_GEM)
            }
            CardType.WEAPON -> {
                color = CardType.WEAPON.color
                generatorDrawable = AssetManager.instance.getDrawable(CastleGame.WARRIOR)
                assetDrawable = AssetManager.instance.getDrawable(CastleGame.SWORD)
            }
            else -> throw IllegalArgumentException("Invalid CardType of value ${cardType.name}")
        }

        val generatorLayoutConstraints = LayoutConstraints(GENERATOR_TEXT_X, GENERATOR_TEXT_Y,
                GENERATOR_TEXT_WIDTH, GENERATOR_TEXT_HEIGHT)
        generatorTextView = TextView(world, generatorLayoutConstraints, AssetManager.instance.getFont(CastleGame.MAIN_FONT).clone())
        generatorTextView.alignment = Align.center
        generatorTextView.textColor = Color.WHITE
        generatorTextView.text = "$generatorCount"
        generatorTextView.setupFontSize()
//        generatorTextView.setPosition(100f, 100f)

        val assetLayoutConstraints = LayoutConstraints(ASSET_TEXT_X, ASSET_TEXT_Y,
                ASSET_TEXT_WIDTH, ASSET_TEXT_HEIGHT)
        assetTextView = TextView(world, assetLayoutConstraints, AssetManager.instance.getFont(CastleGame.MAIN_FONT).clone())
        assetTextView.alignment = Align.center
        assetTextView.textColor = Color.WHITE
        assetTextView.setupFontSize()
        assetTextView.text = "$assetCount"

        addView(generatorTextView)
        addView(assetTextView)
    }

    override fun onDraw(spriteBatch: Batch, viewport: Viewport) {
        super.onDraw(spriteBatch, viewport)

        generatorDrawable.draw(spriteBatch, GENERATOR_X, GENERATOR_Y,
                GENERATOR_WIDTH, GENERATOR_HEIGHT)
        assetDrawable.draw(spriteBatch, ASSET_X, ASSET_Y,
                ASSET_WIDTH, ASSET_HEIGHT)

        generatorTextView.dispatchDraw(spriteBatch, viewport)
        assetTextView.dispatchDraw(spriteBatch, viewport)
    }

    override fun update(delta: Float) {
        super.update(delta)
        generatorColorAnimator?.let {
            it.update(delta)
            generatorTextView.textColor = it.currentColor
        }
        assetColorAnimator?.let {
            it.update(delta)
            assetTextView.textColor = it.currentColor
        }
    }

    fun redOrGreen(field: Int, value: Int) : Color {
        return if (value > field) CastleGame.GREEN else CastleGame.RED
    }

    companion object {
        @JvmStatic val WIDTH: Float = 250f

        @JvmStatic val HEIGHT: Float = 250f
    }
}