package com.desitum.castlewars.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.*
import com.desitum.library.drawing.Drawable
import com.desitum.library.game.AssetManager
import com.desitum.library.game.World
import com.desitum.library.logging.Log
import com.desitum.library.view.*

/**
 * Created by kodyvanry on 7/31/17
 */
class InformationView constructor(gameScreen: GameScreen, layoutConstraints: LayoutConstraints, font: BitmapFont, background: Drawable, var text: String, okText: String = "OK", screenOverlay: Drawable? = null, val onConfirmation: () -> Unit = {}, val delay: Float = 0f) : ViewGroup(gameScreen.world, layoutConstraints), View.OnClickListener {

    var alignment: Int = 0
    private val glyphLayout: GlyphLayout = GlyphLayout(font, text)
    var textColor: Color
    val yesText: TextView
    val font: BitmapFont = font.clone()
    val disableView = View(world, LayoutConstraints(0f, 0f, gameScreen.foregroundViewportWidth, gameScreen.foregroundViewportHeight))

    init {
        val world = gameScreen.world
        screenOverlay?.let {
            disableView.backgroundDrawable = it
            it.color.a = 0f
            disableView.color = it.color
            disableView.fadeIn(delay = delay)
        }
        gameScreen.world.addView(disableView)
        disableView.clickable = true
        disableView.onClickListener = this
        glyphLayout.setText(font, text, Color.BLACK, textWidth, Align.center, true)
        textColor = Color.BLACK
        this.alignment = LinearLayout.ALIGNMENT_CENTER
        backgroundDrawable = background

        yesText = TextView(world, LayoutConstraints(
                layoutConstraints.width / 4f, 0f, layoutConstraints.width / 2f, 100f
        ), font.clone())
        yesText.id = OK
        yesText.text = okText
        yesText.font.color = CastleGame.BLUE
        yesText.textColor = CastleGame.BLUE
        yesText.clickable = true
        yesText.alignment = Align.center
//        yesText.backgroundDrawable = AssetManager.instance.getDrawable(CastleGame.BLACK_CARD)
        yesText.onClickListener = this

        addView(yesText)
        setColor(1f, 1f, 1f, 0f)
    }

    override fun onClick(view: View) {
        when (view.id) {
            OK -> {
                world?.removeView(disableView)
                clearAnimators()
                fadeOut()
                onConfirmation()
            }
        }
    }

    override fun onDraw(batch: Batch, viewport: Viewport) {
        super.onDraw(batch, viewport)
        font.draw(batch, glyphLayout, x + width / 2 - textWidth / 2, y + height / 2 + textHeight / 2f)
    }

    val textWidth: Float
        get() = layoutConstraints.width * .8f

    var _textHeight: Float? = null
    val textHeight: Float
        get() {
            _textHeight?.let { return it }
            glyphLayout.setText(font, text, Color.BLACK, textWidth, Align.center, true)
            _textHeight = glyphLayout.height
            return _textHeight!!
        }

    companion object {
        val OK = 1
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        font?.setColor(textColor.r, textColor.g, textColor.b, a)
        children?.forEach { it.setColor(it.color.r, it.color.g, it.color.b, a) }
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
        glyphLayout?.setText(font, text, Color(0f, 0f, 0f, tint.a), textWidth, Align.center, true)
    }

    override var z: Int = 0
        set(value) {
            disableView?.z = value - 1
            yesText?.z = value + 1
            field = value
        }
}