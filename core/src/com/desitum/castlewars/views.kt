package com.desitum.castlewars

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.utils.viewport.Viewport
import com.desitum.castlewars.view.Card
import com.desitum.castlewars.view.ConfirmationView
import com.desitum.castlewars.view.InformationView
import com.desitum.library.animation.ColorEffects
import com.desitum.library.animation.MovementAnimator
import com.desitum.library.game.AssetManager
import com.desitum.library.game_objects.GameObject
import com.desitum.library.game_objects.Visibility
import com.desitum.library.interpolation.Interpolation
import com.desitum.library.logging.Log
import com.desitum.library.view.LayoutConstraints
import com.desitum.library.view.View
import com.desitum.library.view.ViewGroup
import java.util.*

/**
 * Created by kodyvanry on 7/29/17.
 */

fun View.fadeOut(delay: Float = 0f, duration: Float = .2f) {
    val fadeAnimation = ColorEffects(this, duration, delay, this.color, Color(this.color.r, this.color.g, this.color.b, 0f))
    fadeAnimation.onAnimationFinished = {
        this.visibility = Visibility.GONE
    }
    this.startAnimator(fadeAnimation)
}

fun View.fadeIn(delay: Float = 0f, duration: Float = 0.2f) {
    val fadeAnimation = ColorEffects(this, duration, delay, this.color, Color(this.color.r, this.color.g, this.color.b, 1f))
    this.visibility = Visibility.VISIBLE
    this.startAnimator(fadeAnimation)
}

fun ViewGroup.fadeOut() {
    val fadeAnimation = ColorEffects(this, .2f, 0f, this.color, Color(this.color.r, this.color.g, this.color.b, 0f))
    fadeAnimation.onAnimationFinished = {
        this.visibility = Visibility.GONE
    }
    this.startAnimator(fadeAnimation)

    children.forEach { child -> child.fadeOut() }
}

fun ViewGroup.fadeIn(delay: Float = 0f, duration: Float = .2f) {
    val fadeAnimation = ColorEffects(this, duration, delay, this.color, Color(this.color.r, this.color.g, this.color.b, 1f))
    visibility = Visibility.VISIBLE
    this.startAnimator(fadeAnimation)

    children.forEach { child -> child.fadeIn(delay, duration) }
}

fun View.slideIn(fromSide: Side, viewportWidth: Float, viewportHeight: Float, delay: Float = 0f) {
    this.color = Color(this.color.r, this.color.g, this.color.b, 1f)
    this.visibility = Visibility.VISIBLE
    when (fromSide) {
        Side.LEFT -> startAnimator(MovementAnimator(this, 0.2f, delay, 0f - this.layoutConstraints.width, this.layoutConstraints.x, this.layoutConstraints.y, this.layoutConstraints.y, Interpolation.DECELERATE_INTERPOLATOR))
        Side.RIGHT -> startAnimator(MovementAnimator(this, 0.2f, delay, viewportWidth, this.layoutConstraints.x, this.layoutConstraints.y, this.layoutConstraints.y, Interpolation.DECELERATE_INTERPOLATOR))
        Side.BOTTOM -> startAnimator(MovementAnimator(this, 0.2f, delay, this.layoutConstraints.x, this.layoutConstraints.x, 0f - this.layoutConstraints.height, this.layoutConstraints.y, Interpolation.DECELERATE_INTERPOLATOR))
        Side.TOP -> startAnimator(MovementAnimator(this, 0.2f, delay, this.layoutConstraints.x, this.layoutConstraints.x, viewportHeight, this.layoutConstraints.y, Interpolation.DECELERATE_INTERPOLATOR))
    }
}

fun GameObject.fadeIn() {
    this.visibility = Visibility.VISIBLE
    val fadeAnimation = ColorEffects(this, 0.2f, 0f, Color(this.color.r, this.color.g, this.color.b, 0f), Color(this.color.r, this.color.g, this.color.b, 1f))
    this.addAndStartAnimator(fadeAnimation)
}

fun GameObject.fadeOut() {
    this.visibility = Visibility.VISIBLE
    val fadeAnimation = ColorEffects(this, 0.2f, 0f, this.color, Color(this.color.r, this.color.g, this.color.b, 0f))
    this.addAndStartAnimator(fadeAnimation)
}

fun GameObject.slideIn(fromSide: Side, viewportWidth: Float, viewportHeight: Float, delay: Float = 0f) {
    this.color = Color(this.color.r, this.color.g, this.color.b, 1f)
    this.visibility = Visibility.VISIBLE
    when (fromSide) {
        Side.LEFT -> addAndStartAnimator(MovementAnimator(this, 0.2f, delay, 0f - this.width, this.x, this.y, this.y, Interpolation.DECELERATE_INTERPOLATOR))
        Side.RIGHT -> addAndStartAnimator(MovementAnimator(this, 0.2f, delay, viewportWidth, this.x, this.y, this.y, Interpolation.DECELERATE_INTERPOLATOR))
        Side.BOTTOM -> addAndStartAnimator(MovementAnimator(this, 0.2f, delay, this.x, this.x, 0f - this.height, this.y, Interpolation.DECELERATE_INTERPOLATOR))
        Side.TOP -> addAndStartAnimator(MovementAnimator(this, 0.2f, delay, this.x, this.x, viewportHeight, this.y, Interpolation.DECELERATE_INTERPOLATOR))
    }
}

fun GameScreen.displayConfirmation(question: String, onYes: () -> Unit = {}, onNo: () -> Unit = {}) {
    this.world.views.find { it is ConfirmationView }?.let {
        this.world.removeView(it)
    }
    val cf = ConfirmationView(this , LayoutConstraints(
            world.foregroundViewport.worldWidth / 4,
            world.foregroundViewport.worldHeight / 4 - 50f,
            world.foregroundViewport.worldWidth / 2f,
            world.foregroundViewport.worldHeight / 2f),
            AssetManager.instance.getFont(CastleGame.MAIN_FONT),
            AssetManager.instance.getDrawable(CastleGame.MENU_CARD),
            question,
            onYes = onYes,
            onNo = onNo
    )
    cf.fadeIn()
    cf.startAnimator(MovementAnimator(cf, .2f, 0f,
            cf.layoutConstraints.x,
            cf.layoutConstraints.x,
            cf.layoutConstraints.y,
            cf.layoutConstraints.y + 50f, Interpolation.DECELERATE_INTERPOLATOR))
    world.addView(cf)
}

fun GameScreen.displayInfo(info: String, onConfirmation: () -> Unit) {
    this.world.views.find { it is ConfirmationView }?.let {
        this.world.removeView(it)
    }
    val cf = InformationView(this , LayoutConstraints(
            world.foregroundViewport.worldWidth * .2f,
            world.foregroundViewport.worldHeight * .2f - 50f,
            world.foregroundViewport.worldWidth * .6f,
            world.foregroundViewport.worldHeight * .6f),
            AssetManager.instance.getFont(CastleGame.MAIN_FONT),
            AssetManager.instance.getDrawable(CastleGame.MENU_CARD),
            info,
            onConfirmation = onConfirmation
    )
    cf.fadeIn()
    cf.startAnimator(MovementAnimator(cf, .2f, 0f,
            cf.layoutConstraints.x,
            cf.layoutConstraints.x,
            cf.layoutConstraints.y,
            cf.layoutConstraints.y + 50f, Interpolation.DECELERATE_INTERPOLATOR))
    world.addView(cf)
}

fun GameScreen.displayChangeTurn(player: Int) {
    this.world.views.find { it is ConfirmationView }?.let {
        this.world.removeView(it)
    }
    val drawable = AssetManager.instance.getDrawable(CastleGame.GARBAGE_BODY)
    drawable.color = Color.BLACK
    val cf = InformationView(this , LayoutConstraints(
            world.foregroundViewport.worldWidth * .2f,
            world.foregroundViewport.worldHeight * .2f - 50f,
            world.foregroundViewport.worldWidth * .6f,
            world.foregroundViewport.worldHeight * .6f),
            AssetManager.instance.getFont(CastleGame.MAIN_FONT),
            AssetManager.instance.getDrawable(CastleGame.MENU_CARD),
            "Player $player",
            "PLAY",
            drawable,
            onConfirmation = { this.startTurn() },
            delay = .75f
    )
    cf.z = 10000
    (cf as ViewGroup).fadeIn(delay = .75f)
    cf.startAnimator(MovementAnimator(cf, .2f, 0f,
            cf.layoutConstraints.x,
            cf.layoutConstraints.x,
            cf.layoutConstraints.y,
            cf.layoutConstraints.y + 50f, Interpolation.DECELERATE_INTERPOLATOR))
    world.addView(cf)
}

enum class Side {
    LEFT,
    RIGHT,
    BOTTOM,
    TOP
}