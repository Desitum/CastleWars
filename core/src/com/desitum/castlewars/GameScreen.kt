package com.desitum.castlewars

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.desitum.castlewars.model.*
import com.desitum.castlewars.persistence.*
import com.desitum.castlewars.view.*
import com.desitum.library.animation.ColorEffects
import com.desitum.library.game.AssetManager
import com.desitum.library.game.GameScreen
import com.desitum.library.game_objects.GameObject
import com.desitum.library.view.Button
import com.desitum.library.view.LayoutConstraints
import com.desitum.library.view.ToggleButton
import com.desitum.library.view.View

/**
 * Created by kodyvanry on 6/4/17.
 *
 * Probably going to end up being the actual game.
 */
class GameScreen(val gameInterface: GameInterface) : GameScreen(VIEWPORT_WIDTH, VIEWPORT_HEIGHT, VIEWPORT_WIDTH, VIEWPORT_HEIGHT, GameScreen.ASPECT_FILL), View.OnClickListener, ShoppingResponseListener, ButtonMenuCombo.OnCloseListener {

    lateinit var background: GameObject
    lateinit var grass: GameObject
    lateinit var ground: GameObject
    val deck: Deck
    var playerCastle: Castle = Castle(world)
    var enemyCastle: Castle = Castle(world)
    var garbageCan: GarbageCan
    val player1 = Player(this, false, playerCastle)
    val player2 = Player(this, true, enemyCastle)
    var currentPlayerTurn = player1
    val playMenu: ButtonMenuCombo
    val shoppingMenu: ButtonMenuCombo
    val settingsMenu: ButtonMenuCombo
    val castleWarsSign: View
    val player1ArmyInventory: InventoryBox
    val player1GemInventory: InventoryBox
    val player1BuilderInventory: InventoryBox
    val player2ArmyInventory: InventoryBox
    val player2GemInventory: InventoryBox
    val player2BuilderInventory: InventoryBox
    val inGameSettings: SettingsMenu
    var descriptionView: CardDescriptionView? = null
    val defaultThemeView: MenuItemView
    val flameThemeView: MenuItemView
    val japaneseThemeView: MenuItemView
    val flameShoppingView: ShoppingView
    val japaneseShoppingView: ShoppingView
    val card1ShoppingView: ShoppingView
    val card2ShoppingView: ShoppingView
    val volumeButton: ToggleButton

    val gameOver: Boolean
        get() = player1.castle.castleLife in arrayOf(0, 100) || player2.castle.castleLife in arrayOf(0, 100)
    val player1Win: Boolean
        get() = player1.castle.castleLife == 100 || player2.castle.castleLife == 0
    val tutorialViews = ArrayList<Button>()
    val backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("sound/bensound_epic.mp3"))

    init {
        player1.enemy = player2
        player2.enemy = player1
        garbageCan = GarbageCan(world)

        GameScreenUI.setupUI(this)

        setClearColor(Color(0.8f, .8f, 1f, 0.3f))

        playMenu = world.views.findById(R.id.play_menu) as ButtonMenuCombo
        shoppingMenu = world.views.findById(R.id.shop_menu) as ButtonMenuCombo
        settingsMenu = world.views.findById(R.id.settings_menu) as ButtonMenuCombo
        playMenu.findViewById(R.id.button_easy)?.onClickListener = this
        playMenu.findViewById(R.id.button_hard)?.onClickListener = this
        playMenu.findViewById(R.id.button_pass_and_play)?.onClickListener = this
        playMenu.onClickListener = this
        shoppingMenu.onClickListener = this
        settingsMenu.onClickListener = this

        player1ArmyInventory = world.views.findById(R.id.p_1_army_inventory) as InventoryBox
        player1GemInventory = world.views.findById(R.id.p_1_gem_inventory) as InventoryBox
        player1BuilderInventory = world.views.findById(R.id.p_1_builder_inventory) as InventoryBox
        player2ArmyInventory = world.views.findById(R.id.p_2_army_inventory) as InventoryBox
        player2GemInventory = world.views.findById(R.id.p_2_gem_inventory) as InventoryBox
        player2BuilderInventory = world.views.findById(R.id.p_2_builder_inventory) as InventoryBox
        inGameSettings = world.views.findById(R.id.settings_menu_in_game) as SettingsMenu
        castleWarsSign = world.views.findById(R.id.castle_wars_sign)

        inGameSettings.findViewById(R.id.button_home)?.onClickListener = this
        volumeButton = inGameSettings.findViewById(R.id.button_volume) as ToggleButton
        defaultThemeView = settingsMenu.findViewById(R.id.theme_default) as MenuItemView
        flameThemeView = settingsMenu.findViewById(R.id.theme_flame) as MenuItemView
        japaneseThemeView = settingsMenu.findViewById(R.id.theme_japanese) as MenuItemView
        flameShoppingView = shoppingMenu.findViewById(R.id.shopping_flame) as ShoppingView
        japaneseShoppingView = shoppingMenu.findViewById(R.id.shopping_japanese) as ShoppingView
        card1ShoppingView = shoppingMenu.findViewById(R.id.shopping_card_1) as ShoppingView
        card2ShoppingView = shoppingMenu.findViewById(R.id.shopping_card_2) as ShoppingView
        defaultThemeView.onClickListener = this
        flameThemeView.onClickListener = this
        japaneseThemeView.onClickListener = this
        flameShoppingView.onClickListener = this
        japaneseShoppingView.onClickListener = this
        card1ShoppingView.onClickListener = this
        card2ShoppingView.onClickListener = this
        volumeButton.onClickListener = this

        deck = Deck(world)
        dealCards()

        backgroundMusic.volume = 0.5f
        backgroundMusic.isLooping = true
        backgroundMusic.play()
        volumeButton.toggled = gameInterface.isVolumeOn();
        if (!gameInterface.isVolumeOn()) {
            backgroundMusic.volume = 0f
        }

        gameInterface.setShoppingResponseListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.play_menu -> {
                if ((view as ButtonMenuCombo).state == ButtonMenuCombo.State.MENU) {
                    shoppingMenu.fadeOut()
                    settingsMenu.fadeOut()
                }
            }
            R.id.shop_menu -> {
                if ((view as ButtonMenuCombo).state == ButtonMenuCombo.State.MENU) {
                    playMenu.fadeOut()
                    settingsMenu.fadeOut()
                }
            }
            R.id.settings_menu -> {
                if ((view as ButtonMenuCombo).state == ButtonMenuCombo.State.MENU) {
                    playMenu.fadeOut()
                    shoppingMenu.fadeOut()
                }
            }
            R.id.button_easy -> {
                gameMode = GameMode.EASY
                player2.useAI = true
                transitionFromMenuToGame()
            }
            R.id.button_hard -> {
                gameMode = GameMode.HARD
                player2.useAI = true
                transitionFromMenuToGame()
            }
            R.id.button_pass_and_play -> {
                gameMode = GameMode.PASS_AND_PLAY
                player2.useAI = false
                transitionFromMenuToGame()
            }
            R.id.button_home -> {
                displayConfirmation("Are you sure you would like to go to the home menu? All progress will be lost.",
                        onYes = {
                            transitionFromGameToMenu()
                        }
                )
                inGameSettings.transitionToButton()
            }
            R.id.button_volume -> {
                if (!volumeButton.toggled) {
                    gameInterface.turnVolumeOn()
                    backgroundMusic.volume = 0.5f
                } else {
                    gameInterface.turnVolumeOff()
                    backgroundMusic.volume = 0f
                }
            }
            R.id.theme_default -> {
                setTheme(Theme.DEFAULT)
            }
            R.id.theme_flame -> {
                if (Settings.flamePackPurchased)
                    setTheme(Theme.FLAME)
                else
                    gameInterface.makePurchase(ShoppingSkus.FLAME_PACK)
            }
            R.id.theme_japanese -> {
                if (Settings.japanesePackPurchased)
                    setTheme(Theme.JAPANESE)
                else
                    gameInterface.makePurchase(ShoppingSkus.JAPANESE_PACK)
            }
            R.id.shopping_flame -> {
                gameInterface.makePurchase(ShoppingSkus.FLAME_PACK)
            }
            R.id.shopping_japanese -> {
                gameInterface.makePurchase(ShoppingSkus.JAPANESE_PACK)
            }
            R.id.shopping_card_1 -> {
                gameInterface.makePurchase(ShoppingSkus.EXTRA_SLOT_1)
            }
            R.id.shopping_card_2 -> {
                gameInterface.makePurchase(ShoppingSkus.EXTRA_SLOT_2)
            }
            else -> {
                // DO NOTHING
            }
        }
    }

    override fun onClose(buttonMenuCombo: ButtonMenuCombo) {
        if (buttonMenuCombo == playMenu) {
            shoppingMenu.reset()
            shoppingMenu.slideIn(Side.BOTTOM, viewportWidth, viewportHeight)
            shoppingMenu.reset()
            settingsMenu.reset()
            settingsMenu.slideIn(Side.BOTTOM, viewportWidth, viewportHeight, delay = 0.05f)
            settingsMenu.reset()
        } else if (buttonMenuCombo == shoppingMenu) {
            playMenu.reset()
            playMenu.slideIn(Side.BOTTOM, viewportWidth, viewportHeight)
            playMenu.reset()
            settingsMenu.reset()
            settingsMenu.slideIn(Side.BOTTOM, viewportWidth, viewportHeight, delay = 0.05f)
            settingsMenu.reset()
        } else if (buttonMenuCombo == settingsMenu) {
            playMenu.reset()
            playMenu.slideIn(Side.BOTTOM, viewportWidth, viewportHeight)
            playMenu.reset()
            shoppingMenu.reset()
            shoppingMenu.slideIn(Side.BOTTOM, viewportWidth, viewportHeight, delay = 0.05f)
            shoppingMenu.reset()
        }
    }

    private fun transitionFromMenuToGame() {
        resetGame()
        playerCastle.animateIn()
        enemyCastle.animateIn()
        playMenu.clearAnimators()
        playMenu.fadeOut()
        shoppingMenu.clearAnimators()
        shoppingMenu.fadeOut()
        settingsMenu.clearAnimators()
        settingsMenu.fadeOut()
        castleWarsSign.clearAnimators()
        castleWarsSign.fadeOut()
        garbageCan.animators.clear()
        garbageCan.slideIn(Side.BOTTOM, viewportWidth, viewportHeight)
        player1ArmyInventory.clearAnimators()
        player1ArmyInventory.slideIn(Side.LEFT, viewportWidth, viewportHeight, delay = 0.1f)
        player1GemInventory.clearAnimators()
        player1GemInventory.slideIn(Side.LEFT, viewportWidth, viewportHeight, delay = 0.05f)
        player1BuilderInventory.clearAnimators()
        player1BuilderInventory.slideIn(Side.LEFT, viewportWidth, viewportHeight)
        player2ArmyInventory.clearAnimators()
        player2ArmyInventory.slideIn(Side.RIGHT, viewportWidth, viewportHeight, delay = 0.1f)
        player2GemInventory.clearAnimators()
        player2GemInventory.slideIn(Side.RIGHT, viewportWidth, viewportHeight, delay = 0.05f)
        player2BuilderInventory.clearAnimators()
        player2BuilderInventory.slideIn(Side.RIGHT, viewportWidth, viewportHeight)
        inGameSettings.clearAnimators()
        inGameSettings.slideIn(Side.BOTTOM, viewportWidth, viewportHeight)
        player1.updateForTurn()
        player1.animateHandOnScreen()
        if (!gameInterface.hasShownCardTutorial()) showTutorialItem(player1.hand[0].x + Card.WIDTH / 2, Card.CARD_Y + Card.HEIGHT, Tutorial.CARD_TUTORIAL, .2f)
        if (!gameInterface.hasShownGarbageCanTutorial()) showTutorialItem(105f, 220f, Tutorial.GARBAGE_CAN_TUTORIAL, 1.5f)
        if (!gameInterface.hasShownDragAndDropTutorial()) showTutorialItem(player1.hand.last().x + Card.WIDTH / 2, Card.CARD_Y + Card.HEIGHT, Tutorial.DRAG_AND_DROP_TUTORIAL, 3.0f)
        if (!gameInterface.hasShownResourcesTutorial()) showTutorialItem(150f, GameScreenUI.getInventoryBoxPosition(1, 3, foregroundViewportHeight) + InventoryBox.HEIGHT, Tutorial.RESOURCES_TUTORIAL, 4.5f)
    }

    fun transitionFromGameToMenu() {
        playerCastle.animateOut()
        enemyCastle.animateOut()
        playMenu.reset()
        playMenu.slideIn(Side.BOTTOM, viewportWidth, viewportHeight)
        playMenu.reset()
        shoppingMenu.reset()
        shoppingMenu.slideIn(Side.BOTTOM, viewportWidth, viewportHeight, 0.05f)
        shoppingMenu.reset()
        settingsMenu.reset()
        settingsMenu.slideIn(Side.BOTTOM, viewportWidth, viewportHeight, 0.1f)
        settingsMenu.reset()
        castleWarsSign.slideIn(Side.TOP, viewportWidth, viewportHeight)
        garbageCan.fadeOut()
        player1ArmyInventory.fadeOut()
        player1GemInventory.fadeOut()
        player1BuilderInventory.fadeOut()
        player2ArmyInventory.fadeOut()
        player2GemInventory.fadeOut()
        player2BuilderInventory.fadeOut()
        inGameSettings.fadeOut()
        player1.updateForTurn()
        player1.animateHandOffScreen()
        tutorialViews.forEach {
            world.removeView(it)
        }
        tutorialViews.clear()
    }

    override fun update(delta: Float) {
        super.update(delta)

        player1.update(delta)
        player2.update(delta)

        player1ArmyInventory.generatorCount = player1.armyMen
        player1ArmyInventory.assetCount = player1.weapons
        player1GemInventory.generatorCount = player1.wizards
        player1GemInventory.assetCount = player1.gems
        player1BuilderInventory.generatorCount = player1.builders
        player1BuilderInventory.assetCount = player1.stones
        player2ArmyInventory.generatorCount = player2.armyMen
        player2ArmyInventory.assetCount = player2.weapons
        player2GemInventory.generatorCount = player2.wizards
        player2GemInventory.assetCount = player2.gems
        player2BuilderInventory.generatorCount = player2.builders
        player2BuilderInventory.assetCount = player2.stones
    }

    fun useCard(card: Card) {
        if (currentPlayerTurn == player1) {
            player1.useCard(card, false)
            player2.useCard(card, true)
            card.commands(player1, player2)
        } else {
            player1.useCard(card, true)
            player2.useCard(card, false)
            card.commands(player2, player1)
        }
    }

    fun endTurn() {
        if (gameOver) {
            val notification = if (player1Win && gameMode != GameMode.PASS_AND_PLAY) {
                "YOU WON!!!"
            } else if (player1Win) {
                "PLAYER 1 WINS!!!"
            } else if (!player1Win && gameMode != GameMode.PASS_AND_PLAY) {
                "YOU LOST..."
            } else {
                "PLAYER 2 WINS!!!"
            }
            displayConfirmation("$notification\n\nWould you like to play again?",
                    onYes = {
                        resetGame()
                    },
                    onNo = {
                        transitionFromGameToMenu()
                    }
            )
        } else {
            currentPlayerTurn.animateHandOffScreen()
            currentPlayerTurn = if (currentPlayerTurn == player1) {
                player2
            } else {
                player1
            }
            if (gameMode == GameMode.PASS_AND_PLAY)
                displayChangeTurn(if (currentPlayerTurn == player1) 1 else 2)
            else
                startTurn()
        }
    }

    fun startTurn() {
//        if (currentPlayerTurn != player2 || gameMode == GameMode.PASS_AND_PLAY)
        currentPlayerTurn.animateHandOnScreen()
        currentPlayerTurn.updateForTurn()
    }

    fun showDescription(card: Card) {
        val lc = LayoutConstraints(card.x, card.y + card.height + CardDescriptionView.ARROW_SIZE, card.width, card.height)
        descriptionView = CardDescriptionView(world, lc, AssetManager.instance.getFont(CastleGame.MAIN_FONT), card.description)
        world.addView(descriptionView!!)
        descriptionView!!.startAnimator(ColorEffects(descriptionView, 0.2f, 0f, Color(1f, 1f, 1f, 0f), Color.WHITE))
    }

    private fun showTutorialItem(centerX: Float, y: Float, tutorial: Tutorial, fadeInDelay: Float) {
        val lc = LayoutConstraints(centerX - TUTORIAL_WIDTH / 2, y, TUTORIAL_WIDTH, TUTORIAL_HEIGHT)
        val tutorialView = Button(world, lc)
        tutorialView.restDrawable = AssetManager.instance.getDrawable(CastleGame.TUTORIAL)
        tutorialView.onClick = {
            displayInfo(tutorial.info) { }
            it.fadeOut()
            when (tutorial) {

                Tutorial.CARD_TUTORIAL -> gameInterface.showCardTutorial()
                Tutorial.GARBAGE_CAN_TUTORIAL -> gameInterface.showGarbageCanTutorial()
                Tutorial.DRAG_AND_DROP_TUTORIAL -> gameInterface.showDragAndDropTutorial()
                Tutorial.RESOURCES_TUTORIAL -> gameInterface.showResourcesTutorial()
            }
        }
        tutorialViews.add(tutorialView) // THIS IS SO WE CAN CLEAR OUT THE TUTORIAL VIEWS WHEN YOU LEAVE THE GAME
        world.addView(tutorialView)
        tutorialView.color = Color(1f, 1f, 1f, 0f)
        tutorialView.fadeIn(delay = fadeInDelay)
    }

    fun dismissDescription() {
        descriptionView?.let {
            world.removeView(it)
        }
    }

    private fun resetGame() {
        player1.reset()
        player2.reset()
        arrayOf(player1, player2).forEach { player ->
            player.hand.forEach {
                deck.discard(it)
                world.removeGameObject(it)
            }
            player.hand.clear()
        }
        dealCards()
        playerCastle.animateIn()
        enemyCastle.animateIn()
        currentPlayerTurn = player1
        player1.updateForTurn()
        player1.animateHandOnScreen()
    }

    private fun dealCards() {
        for (i in 1..Settings.HAND_SIZE) {
            player1.addCardToHand(deck.drawCard())
            player2.addCardToHand(deck.drawCard())
        }
    }

    private fun setTheme(theme: Theme) {
        when (theme) {
            Theme.DEFAULT -> {
                playerCastle.castle = AssetManager.instance.getDrawable(CastleGame.CASTLE)
                playerCastle.wall = AssetManager.instance.getDrawable(CastleGame.WALL)
                playerCastle.doorVisible = true
                enemyCastle.castle = AssetManager.instance.getDrawable(CastleGame.CASTLE)
                enemyCastle.wall = AssetManager.instance.getDrawable(CastleGame.WALL)
                enemyCastle.doorVisible = true
                background.drawable = AssetManager.instance.getDrawable(CastleGame.SKY)
                grass.drawable = AssetManager.instance.getDrawable(CastleGame.GRASS)
                ground.drawable = AssetManager.instance.getDrawable(CastleGame.GROUND)
                defaultThemeView.highlighted = true
                flameThemeView.highlighted = false
                japaneseThemeView.highlighted = false
            }
            Theme.FLAME -> {
                playerCastle.castle = AssetManager.instance.getDrawable(CastleGame.FLAME_CASTLE)
                playerCastle.wall = AssetManager.instance.getDrawable(CastleGame.BLANK_WALL)
                playerCastle.doorVisible = false
                enemyCastle.castle = AssetManager.instance.getDrawable(CastleGame.FLAME_CASTLE)
                enemyCastle.wall = AssetManager.instance.getDrawable(CastleGame.BLANK_WALL)
                enemyCastle.doorVisible = false
                background.drawable = AssetManager.instance.getDrawable(CastleGame.FLAME_SKY)
                grass.drawable = AssetManager.instance.getDrawable(CastleGame.FLAME_GRASS)
                ground.drawable = AssetManager.instance.getDrawable(CastleGame.FLAME_GROUND)
                defaultThemeView.highlighted = false
                flameThemeView.highlighted = true
                japaneseThemeView.highlighted = false
            }
            Theme.JAPANESE -> {
                playerCastle.castle = AssetManager.instance.getDrawable(CastleGame.JAPAN_CASTLE)
                playerCastle.wall = AssetManager.instance.getDrawable(CastleGame.BLANK_WALL)
                playerCastle.doorVisible = false
                enemyCastle.castle = AssetManager.instance.getDrawable(CastleGame.JAPAN_CASTLE)
                enemyCastle.wall = AssetManager.instance.getDrawable(CastleGame.BLANK_WALL)
                enemyCastle.doorVisible = false
                background.drawable = AssetManager.instance.getDrawable(CastleGame.JAPAN_SKY)
                grass.drawable = AssetManager.instance.getDrawable(CastleGame.GRASS)
                ground.drawable = AssetManager.instance.getDrawable(CastleGame.GROUND)
                defaultThemeView.highlighted = false
                flameThemeView.highlighted = false
                japaneseThemeView.highlighted = true
            }
        }
    }

    override fun onShoppingResponse(skus: List<Purchase>) {
        skus.forEach {
            println(it.sku)
        }
        Settings.setPurchases(skus)
        player1.emptyHand()
        player2.emptyHand()
        deck.reload()
        dealCards()
        flameShoppingView.purchased = Settings.flamePackPurchased
        japaneseShoppingView.purchased = Settings.japanesePackPurchased
        card1ShoppingView.purchased = Settings.extraSlot1Purchased
        card2ShoppingView.purchased = Settings.extraSlot2Purchased
    }

    override fun onPurchaseResponse(sku: String) {
        Settings.addPurchase(sku)
        player1.emptyHand()
        player2.emptyHand()
        deck.reload()
        dealCards()
        flameShoppingView.purchased = Settings.flamePackPurchased
        japaneseShoppingView.purchased = Settings.japanesePackPurchased
        card1ShoppingView.purchased = Settings.extraSlot1Purchased
        card2ShoppingView.purchased = Settings.extraSlot2Purchased
    }

    companion object {
        val VIEWPORT_WIDTH = 1920f
        val VIEWPORT_HEIGHT = 1080f
        val TUTORIAL_WIDTH = 113f * .8f
        val TUTORIAL_HEIGHT = 129f * .8f

        var gameMode = GameMode.EASY

        enum class GameMode {
            EASY,
            HARD,
            PASS_AND_PLAY
        }
    }
}
