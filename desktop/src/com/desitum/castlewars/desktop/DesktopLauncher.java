package com.desitum.castlewars.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.desitum.castlewars.CastleGame;
import com.desitum.castlewars.persistence.Purchase;
import com.desitum.castlewars.persistence.GameInterface;
import com.desitum.castlewars.persistence.ShoppingResponseListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 640;
		config.height = 480;
		config.samples = 5;
		new LwjglApplication(new CastleGame(new GameInterface() {
			@Override
			public void turnVolumeOff() {

			}

			@Override
			public void turnVolumeOn() {

			}

			@Override
			public boolean isVolumeOn() {
				return false;
			}

			@Override
			public void showResourcesTutorial() {

			}

			@Override
			public void showDragAndDropTutorial() {

			}

			@Override
			public void showGarbageCanTutorial() {

			}

			@Override
			public void showCardTutorial() {

			}

			@Override
			public boolean hasShownResourcesTutorial() {
				return false;
			}

			@Override
			public boolean hasShownDragAndDropTutorial() {
				return false;
			}

			@Override
			public boolean hasShownGarbageCanTutorial() {
				return false;
			}

			@Override
			public boolean hasShownCardTutorial() {
				return false;
			}

			@Override
			public void makePurchase(String sku) {

			}

			@NotNull
			@Override
			public List<Purchase> getPurchases() {
				return null;
			}

			@Override
			public void setShoppingResponseListener(ShoppingResponseListener responseListener) {

			}

			@NotNull
			@Override
			public ShoppingResponseListener getShoppingResponseListener() {
				return null;
			}
		}), config);
	}
}
