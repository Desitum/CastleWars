package com.desitum.castleWars.android;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.desitum.castlewars.CastleGame;
import com.desitum.castlewars.persistence.Purchase;
import com.desitum.castlewars.persistence.GameInterface;
import com.desitum.castlewars.persistence.ShoppingResponseListener;
import com.desitum.castlewars.persistence.ShoppingSkus;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AndroidLauncher extends AndroidApplication implements GameInterface, GetPurchasesAsyncTask.OnPurchasesResponseListener {

    private static final String DEVICE_UUID = "d_uuid";
    private static final String CARD_TUTORIAL = "CARD_TUTORIAL";
    private static final String GARBAGE_CAN_TUTORIAL = "GARBAGE_CAN_TUTORIAL";
    private static final String DRAG_AND_DROP_TUTORIAL = "DRAG_AND_DROP_TUTORIAL";
    private static final String RESOURCES_TUTORIAL = "RESOURCES_TUTORIAL";
    private static final String VOLUME = "volume";

    private IInAppBillingService mService;

    private List<Purchase> purchases = new ArrayList<>();

    private ShoppingResponseListener shoppingResponseListener;

    private boolean shownCardTutorial = false;
    private boolean shownGarbageCanTutorial = false;
    private boolean shownDragAndDropTutorial = false;
    private boolean shownResourcesTutorial = false;
    private boolean volumeOn = true;

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            try {
                Bundle ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);
                onPurchasesResponse(ownedItems);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new CastleGame(this), config);
        Intent serviceIntent =
                new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
        loadPurchases();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            unbindService(mServiceConn);
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void makePurchase(String sku) {
        try {
            Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(),
                    sku, "inapp", getUUID());

            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");

            startIntentSenderForResult(pendingIntent.getIntentSender(),
                    1001, new Intent(), 0, 0,
                    0);
        } catch (RemoteException | IntentSender.SendIntentException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    @Override
    public List<Purchase> getPurchases() {
        return purchases;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");
                    boolean found = false;
                    for (Purchase purchase : purchases) {
                        if (purchase.getSku().equals(sku))
                            found = true;
                    }
                    shoppingResponseListener.onPurchaseResponse(sku);
                    if (!found)
                        purchases.add(new Purchase(sku));
//                    alert("You have bought the " + sku + ". Excellent choice,
//                            adventurer!");
                }
                catch (JSONException e) {
//                    alert("Failed to parse purchase data.");
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressLint("ApplySharedPref")
    private String getUUID() {
        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (!sharedPreferences.contains(DEVICE_UUID)) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(DEVICE_UUID, UUID.randomUUID().toString());
            editor.commit();
        }
        return sharedPreferences.getString(DEVICE_UUID, UUID.randomUUID().toString());
    }

    private void loadPurchases() {
        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (sharedPreferences.getBoolean(ShoppingSkus.getFLAME_PACK(), false))
            purchases.add(new Purchase(ShoppingSkus.getFLAME_PACK()));
        if (sharedPreferences.getBoolean(ShoppingSkus.getJAPANESE_PACK(), false))
            purchases.add(new Purchase(ShoppingSkus.getJAPANESE_PACK()));
        if (sharedPreferences.getBoolean(ShoppingSkus.getEXTRA_SLOT_1(), false))
            purchases.add(new Purchase(ShoppingSkus.getEXTRA_SLOT_1()));
        if (sharedPreferences.getBoolean(ShoppingSkus.getEXTRA_SLOT_2(), false))
            purchases.add(new Purchase(ShoppingSkus.getEXTRA_SLOT_2()));
        shownCardTutorial = sharedPreferences.getBoolean(CARD_TUTORIAL, shownCardTutorial);
        shownGarbageCanTutorial = sharedPreferences.getBoolean(GARBAGE_CAN_TUTORIAL, shownGarbageCanTutorial);
        shownDragAndDropTutorial = sharedPreferences.getBoolean(DRAG_AND_DROP_TUTORIAL, shownDragAndDropTutorial);
        shownResourcesTutorial = sharedPreferences.getBoolean(RESOURCES_TUTORIAL, shownResourcesTutorial);
        volumeOn = sharedPreferences.getBoolean(VOLUME, volumeOn);
    }

    private void savePersistence() {
        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(ShoppingSkus.getFLAME_PACK(), isPurchased(ShoppingSkus.getFLAME_PACK()));
        editor.putBoolean(ShoppingSkus.getJAPANESE_PACK(), isPurchased(ShoppingSkus.getJAPANESE_PACK()));
        editor.putBoolean(ShoppingSkus.getEXTRA_SLOT_1(), isPurchased(ShoppingSkus.getEXTRA_SLOT_1()));
        editor.putBoolean(ShoppingSkus.getEXTRA_SLOT_2(), isPurchased(ShoppingSkus.getEXTRA_SLOT_2()));
        editor.putBoolean(CARD_TUTORIAL, shownCardTutorial);
        editor.putBoolean(GARBAGE_CAN_TUTORIAL, shownGarbageCanTutorial);
        editor.putBoolean(DRAG_AND_DROP_TUTORIAL, shownDragAndDropTutorial);
        editor.putBoolean(RESOURCES_TUTORIAL, shownResourcesTutorial);
        editor.putBoolean(VOLUME, volumeOn);
        editor.apply();
    }

    private boolean isPurchased(String sku) {
        for (Purchase purchase : purchases) {
            if (purchase.getSku().equals(sku))
                return true;
        }
        return false;
    }

    @Override
    public void onPurchasesResponse(Bundle ownedItems) {
        int response = ownedItems.getInt("RESPONSE_CODE");
        if (response == 0) {
            ArrayList<String> ownedSkus =
                    ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
            ArrayList<String>  purchaseDataList =
                    ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
            ArrayList<String>  signatureList =
                    ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
            String continuationToken =
                    ownedItems.getString("INAPP_CONTINUATION_TOKEN");

            for (int i = 0; i < purchaseDataList.size(); ++i) {
                String sku = ownedSkus.get(i);
                purchases.add(new Purchase(sku));
                // do something with this purchase information
                // e.g. display the updated list of products owned by user
            }

            // if continuationToken != null, call getPurchases again
            // and pass in the token to retrieve more items
        }
        savePersistence();
        if (shoppingResponseListener != null)
            shoppingResponseListener.onShoppingResponse(purchases);
    }

    @Override
    public void setShoppingResponseListener(@NotNull ShoppingResponseListener responseListener) {
        shoppingResponseListener = responseListener;
        shoppingResponseListener.onShoppingResponse(purchases);
    }

    @NotNull
    @Override
    public ShoppingResponseListener getShoppingResponseListener() {
        return shoppingResponseListener;
    }

    @Override
    public boolean hasShownCardTutorial() {
        return shownCardTutorial;
    }

    @Override
    public boolean hasShownGarbageCanTutorial() {
        return shownGarbageCanTutorial;
    }

    @Override
    public boolean hasShownDragAndDropTutorial() {
        return shownDragAndDropTutorial;
    }

    @Override
    public boolean hasShownResourcesTutorial() {
        return shownResourcesTutorial;
    }

    @Override
    public void showCardTutorial() {
        shownCardTutorial = true;
        savePersistence();
    }

    @Override
    public void showGarbageCanTutorial() {
        shownGarbageCanTutorial = true;
        savePersistence();
    }

    @Override
    public void showDragAndDropTutorial() {
        shownDragAndDropTutorial = true;
        savePersistence();
    }

    @Override
    public void showResourcesTutorial() {
        shownResourcesTutorial = true;
        savePersistence();
    }

    @Override
    public boolean isVolumeOn() {
        return volumeOn;
    }

    @Override
    public void turnVolumeOn() {
        volumeOn = true;
        savePersistence();
    }

    @Override
    public void turnVolumeOff() {
        volumeOn = false;
        savePersistence();
    }
}
