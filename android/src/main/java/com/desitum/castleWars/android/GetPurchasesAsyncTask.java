package com.desitum.castleWars.android;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;

import com.android.vending.billing.IInAppBillingService;

/**
 * Created by kodyvanry on 8/16/17
 */
public class GetPurchasesAsyncTask extends AsyncTask<IInAppBillingService, IInAppBillingService, Bundle> {

    private String packageName;
    private OnPurchasesResponseListener responseListener;

    public GetPurchasesAsyncTask(String packageName, OnPurchasesResponseListener responseListener) {
        this.packageName = packageName;
        this.responseListener = responseListener;

    }

    @Override
    protected Bundle doInBackground(IInAppBillingService... params) {
        try {
            return params[0].getPurchases(3, packageName, "inapp", null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bundle bundle) {
        super.onPostExecute(bundle);
        responseListener.onPurchasesResponse(bundle);
    }

    public interface OnPurchasesResponseListener {
        void onPurchasesResponse(Bundle bundle);
    }
}
